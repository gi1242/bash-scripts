#! /bin/bash
# Convert DVI to PDF.
# Modified by Gautam Iyer <gautam@math.uchicago.edu> to include paper sizes
#
# Please contact Andrew Ford <A.Ford@ford-mason.co.uk> with any questions
# about this file.
#
# Based on ps2pdf

OPTIONS=""
dvipsOpt=""
while true; do
    case "$1" in
	-sPAPERSIZE=*)
	    dvipsOpt="-t${1#-sPAPERSIZE=}"
	    OPTIONS="$OPTIONS $1"
	    ;;

	-*) OPTIONS="$OPTIONS $1" ;;
	*)  break ;;
    esac
    shift
done

if (( $# < 1 || $# > 2 )); then
    echo "Usage: `basename $0` [options...] input.dvi [output.pdf]" 1>&2
    exit 1
fi

infile=$1;

if (( $# == 1 )); then
    outfile=${1%.dvi}.pdf
else
    outfile=$2
fi

# We have to include the options twice because -I only takes effect if it
# appears before other options.
# [gghibo] 20010906: force using -P generic flag for better PDF files.
# exec dvips -j0 -P generic -q -f "$infile" | gs $OPTIONS -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="$outfile" $OPTIONS -dCompatibilityLevel=1.3 -c .setpdfwrite -
dvips $dvipsOpt -f "$infile" | exec gs $OPTIONS -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile="$outfile" $OPTIONS -dCompatibilityLevel=1.4 -c .setpdfwrite -
