#! /bin/bash
# Rsync important directories to a different hard disk (just in case the worst
# happens).

# Default to $1, or /backup/daily/home1 on odd days and /backup/daily/home2 on
# even days
# rsync_dir=${1:-"/backup/daily/home$(( 10#$(date '+%j') % 2 ))"}

# Better to make incremental backups
nice rsync -a --delete --delete-excluded --exclude-from - \
    --backup --backup-dir=/backup/tmp/daily-$(date "+%m%d%H") \
    /home/ /backup/home-daily/ <<- EOF
	+ src/
	+ etc/
	+ mail/
	+ math/
	+ per/
	+ config/
	- /*/*
EOF
