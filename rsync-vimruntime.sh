#! /bin/bash

# Sync runtime files
rsync -avzcP --delete --exclude="dos" ftp.nluug.nl::Vim/runtime/ /opt/pkgs/vim63/share/vim/vim63

# Sync patches directory
# rsync -avzcP --delete --exclude="dos" ftp.nluug.nl::Vim/patches/6.3/ /opt/src/vimpatches/
