#! /bin/zsh
# Created   : Sun 26 Oct 2014 08:31:53 PM EDT
# Modified  : Sun 10 May 2020 12:34:47 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Description: Monitor battery charge

zmodload zsh/mathfunc
float NEW_ENERGY_NOW OLD_ENERGY_NOW

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

uevent=/sys/class/power_supply/BAT0/uevent
DELAY=10
USE_ACPI=0
LOGFILE=$HOME/tmp/battery-log.csv
backlight=/sys/class/backlight/intel_backlight
while getopts 'd:al:u:' OPT; do
    case $OPT in
	('d')	DELAY=$OPTARG;;
	('a')	USE_ACPI=1;;
	('l')	LOGFILE=$OPTARG;;
	('u')	uevent=$OPTARG;;
    esac
done

[[ -z $DISPLAY ]] && export DISPLAY=:0

echo "Press Ctrl-C to stop" >> /dev/stderr
if [[ $USE_ACPI != 1 && -f $uevent ]]; then
    OLD_STATUS=
    OLD_ENERGY_NOW=
    START_TIME=
    while ((1)); do
	if [[ -z $OLD_STATUS || $CUR_TIME != "$START_TIME" ]]; then
	    eval $(sed -e "s/POWER_SUPPLY_\(.*\)=\(.*\)$/NEW_\1='\2'/" \
		< $uevent)
	fi

	if [[ -z $OLD_STATUS ]]; then
	    OLD_STATUS=$NEW_STATUS
	    OLD_ENERGY_NOW=$NEW_ENERGY_NOW
	    START_TIME=$(date +%s)
	    CUR_TIME=$START_TIME
	    continue
	elif [[ $OLD_STATUS == Discharging && $CUR_TIME != "$START_TIME" ]];
	then
	    dr_p=$(( (OLD_ENERGY_NOW-NEW_ENERGY_NOW)/NEW_ENERGY_FULL*100 ))
	    dr_m=$(( (CUR_TIME - START_TIME)/60.0 ))
	    (( dr_p == 0 )) && dr_p=0.00001
	    
	    printf "%s BAT0: %5.2f%%, %2d:%02d left, %2d:%02d total. Drained %.2f%% in %.1f min.\n" \
		"$(date +'%T')" 'NEW_ENERGY_NOW/NEW_ENERGY_FULL*100' \
		'NEW_POWER_NOW ? NEW_ENERGY_NOW/NEW_POWER_NOW : 0' \
		'NEW_POWER_NOW ? (NEW_ENERGY_NOW/NEW_POWER_NOW % 1)*60 : 0' \
		'dr_m / dr_p * 100.0 / 60' 'dr_m / dr_p * 100.0 % 60' \
		$dr_p $dr_m
	else
	    date +"%F %T: $(acpi -b)"
	fi

	percent=$(( int(NEW_ENERGY_NOW/NEW_ENERGY_FULL*100) ))
	#if [[ $NEW_STATUS == Charging ]] && (( percent >= 80 )); then
	#    notify-send -u low -i battery-symbolic 'Unplug Charger' \
	#	"Battery at $percent%"
	#elif
	## if [[ $NEW_STATUS == Discharging ]] && (( percent <= 40 )); then
	##     notify-send -u low -i battery-symbolic 'Plug Charger' \
	## 	"Battery at $percent%"
	## fi

	if [[ -n $LOGFILE ]]; then
	    loadavg=($(</proc/loadavg))
	    if [[ -r $backlight/brightness ]]; then
		brightness=$(<$backlight/brightness)
	    else
		brightness=0
	    fi
	    # Fields in logfile:
	    #	1. Current time (seconds since epoch)
	    # 	2. Battery percent
	    # 	3. Load average (last 1 min)
	    # 	4. Load average (last 5 mins)
	    # 	5. Load average (last 10 mins)
	    # 	6. # currently running processes / total processes
	    # 	7. Brightness
	    printf "%d,%f,%s,%s,%s,%s,%d\n" \
		$CUR_TIME 'NEW_ENERGY_NOW/NEW_ENERGY_FULL*100' \
		$loadavg[1,4] \
		$brightness \
		>> $LOGFILE
	fi

	sleep $DELAY
	OLD_TIME=$CUR_TIME
	CUR_TIME=$(date +%s)

	if [[ $NEW_STATUS != $OLD_STATUS ]] || \
		(( $CUR_TIME - $OLD_TIME > $DELAY + 10 ));
	then
	    echo "${BD}Status change or suspend. Resetting stats.${RE}"
	    OLD_STATUS=
	    continue
	fi
    done
else
    while ((1)); do 
	date +"%F %T %Z: $(acpi -b)"
	sleep $DELAY
    done
fi
