#! /bin/sh
# Created   : Thu 03 Dec 2020 10:38:02 AM EST
# Modified  : Thu 03 Dec 2020 10:38:37 AM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
# Prints packages that provide libraries in a given executable.
# Taken from https://unix.stackexchange.com/questions/101824/is-there-a-way-to-determine-what-packages-or-libraries-should-be-loaded-to-suppo

if [ -z $1 ]; then
    echo "Usage: $0 <executable>"
    exit 1
fi

ldd $1 | awk '/=>/{print $(NF-1)}' | \
    while read n; do dpkg-query -S $n; done | \
    sed 's/^\([^:]\+\):.*$/\1/' | uniq
