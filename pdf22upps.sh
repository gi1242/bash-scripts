#! /bin/bash
# Created   : Fri 21 Nov 2008 12:13:04 PM PST
# Modified  : Tue 01 Dec 2009 04:36:58 PM EST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Takes a PDF on 'half letter' paper (e.g. all my notes from Xournal), and
# converts it to a 2up'ed PS file on letter paper for easy printing.

if [[ -z "$1" || ! -f "$1" ]]; then
    echo "Converts a PDF file on 'half letter' paper to a 2 up'ed PS file on"
    echo "letter paper. (Writes PS file on stdout)"
    exit 1;
fi

pdftops "$1" - | psnup -pletter -W5.5in -H8.5in -2
