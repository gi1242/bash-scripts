#! /bin/bash
# Created   : Mon 30 Jul 2007 10:29:48 PM EDT
# Modified  : Mon 30 Jul 2007 10:29:48 PM EDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
# 
# Wait some time and display a dialog

function print_help()
{
    echo "Usage: wait_msg -w <time> -m <message>"
    exit 1;
}

sleepTime=
msg=

while getopts "w:m:" OPT; do
    case $OPT in
	"w") sleepTime=${OPTARG};;
	"m") msg=$OPTARG;;
	"?") print_help;;
    esac
done

[[ -z $sleepTime || -z $msg ]] && print_help

which Xdialog >& /dev/null && dialog=Xdialog
[[ -z $dialog ]] && which kdialog >& /dev/null && dialog=kdialog
if [[ -z $dialog ]]; then
    echo Unable to find Xdialog or kdialog
    exit 1
fi

sleep $sleepTime
$dialog --msgbox "$msg" 0x0

