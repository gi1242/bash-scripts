#! /bin/bash

while [[ -n $1 && $1 != "-e" ]]; do
    shift
done

if [[ -z $1 ]]; then
    echo bad usage
    exit 1
fi

shift
exec $*
