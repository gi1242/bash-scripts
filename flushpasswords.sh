#! /bin/bash
# Created   : Wed 28 Dec 2011 01:14:02 AM EST
# Modified  : Sun 10 May 2015 08:42:52 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Flush all keys/passwords from memory (except ssh)

# kwallet
if which closewallets >& /dev/null; then
    closewallets
else
    echo "Unable to close KWallet"
fi

pkill -u $UID -SIGHUP gpg-agent

# Unmount ~/crypt
grep -q '/home/gautam/crypt' /proc/mounts && fusermount -u crypt

# Unmount ~/Private and clear keys
if [[ -f /dev/shm/ecryptfs-gautam-Private ]] && \
    (( $(< /dev/shm/ecryptfs-gautam-Private) > 1 ));
then
    echo 1 > /dev/shm/ecryptfs-gautam-Private
fi
ecryptfs-umount-private
