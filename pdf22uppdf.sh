#! /bin/bash
# Created   : Sun 05 Apr 2009 12:13:49 AM PDT
# Modified  : Sun 05 Apr 2009 12:28:17 AM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Takes a PDF on 'half letter' paper (e.g. all my notes from Xournal), and
# converts it to a 2up'ed PDF file on letter paper for easy hosting.
# These don't seem to print so well though, so use the pdf22upps instead for
# printing.

if [[ $# != 2 || ! -f "$1" ]]; then
    echo "Converts a PDF file on 'half letter' paper to a 2 up'ed PDF file on"
    echo "letter paper."
    exit 1;
fi

input="$1"
output="$2"

if [[ -d $output ]]; then
    output=${output}/${input##*/}
fi

if [[ -f $output ]]; then
    read -p "File $output exists. Overwrite? " confirm
    [[ $confirm != y && $confirm != yes ]] && exit 1
fi

pdfnup --nup 2x1 --paper letterpaper $input --outfile $output
