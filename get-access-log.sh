#! /bin/bash
# Created   : Sat 05 Feb 2011 02:28:46 PM EST
# Modified  : Thu 01 Oct 2015 04:22:10 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
# 
# Get 2 months worth of Apache access logs from qwe0

#ssh root@qwe0 exec zsh << EOF > ~/tmp/access_log
#    zgrep --no-filename '/~gautam/' /var/log/apache2/access_log{-*(m-60),} | \
#	egrep -v '(bot|gsa-crawler)'
#EOF
ssh -T root@qwe0 << EOF > ~/tmp/access_log
    zgrep --no-filename '/~gautam/' /var/log/apache2/access.log{.2.gz,.1,} | \
	egrep -v '(bot|gsa-crawler)'
EOF
