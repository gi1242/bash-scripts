#! /bin/bash
# mkpassport.sh image width outfile
# image is a square image (your passport photo). width is the width of the
# image in pixels. outputs a correctly bordered image containing 6 copies of
# your passport photo.

# Note:  Kosiks's print out 4 by 5+31/32" pictures (not 4 by 6). So a border
# needs to be added to make the resolution correct.

if [[ $1 == "--square" ]]; then
    # Square passport image
    # parameters: width infile outfile
    shift
    width=$1
    image=$2
    outfile=$3

    # Resolution = width / 2 ( = 3*width/6)
    # Pad 31/64" in the x direction.
    # Note 5+31/32 = 191/32
    resolution=$(( width / 2 ))
    xpad=$((resolution / 32)) # 1/32 inch
    ypad=$((resolution / 32)) # 1/32 inch

    echo "Final size $((3*width + 2*xpad))x$((2*width + 2*ypad)) (padded by $((2*xpad)), $((2*ypad)))"

    # Create the image
    montage -mode concatenate -tile 3x2 -bordercolor '#000000' \
	-border 1x1 $image $image $image $image $image $image jpg:- | \
	convert -border ${xpad}x${ypad} -bordercolor '#000000' jpg:- $outfile
elif [[ $1 == '--narrow' ]]; then
    # Rectangular image
    # parameters: width height infile outfile
    # TODO: Fix the aspect ratio. For now, we produce a 35x45mm image
    shift
    width=$1
    height=$2
    infile=$3
    outfile=$4

    # XRes=width/35 ppmm, YRes=height/45 ppmm.
    # XPad=(6in-14cm)/2 = 6.2*XRes
    # YPad=(4in-9cm)/2 = 5.8*YRes
    xpad=$((31*width/175))
    ypad=$((29*height/225))

    echo "Final size $((4*width + 2*xpad))x$((2*height + 2*ypad)) (padded by $((2*xpad)), $((2*ypad)))"
    montage -mode concatenate -tile 4x2 $infile $infile $infile $infile $infile $infile $infile $infile png:- | convert -border ${xpad}x${ypad} -bordercolor '#808080' png:- $outfile
fi
