#! /bin/bash
# Created   : Thu 21 Sep 2017 08:51:45 AM EDT
# Modified  : Wed 24 Feb 2021 10:10:16 AM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

MAILDIR=$HOME/mail

if [[ -s $MAILDIR/rspam ]]; then
    date '+%F %T: Relearning spam' >> ~/.spamassassin/relearn.log
    sa-learn --mbox --spam $MAILDIR/rspam >> ~/.spamassassin/relearn.log

    # Create a list of from addresses to possibly blacklist later
    formail -s formail -x From: < $MAILDIR/rspam >> ~/.spamassassin/spammers

    echo -n > $MAILDIR/rspam
fi

if [[ -s $MAILDIR/rham ]]; then
    date '+%F %T: Relearning ham' >> ~/.spamassassin/relearn.log
    sa-learn --mbox --ham $MAILDIR/rham >> ~/.spamassassin/relearn.log
    echo -n > $MAILDIR/rham
fi
