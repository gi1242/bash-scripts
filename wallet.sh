#! /bin/bash
# mkpassport.sh image width outfile
# image is a square image (your passport photo). width is the width of the
# image in pixels. outputs a correctly bordered image containing 6 copies of
# your passport photo.

# Note:  Kosiks's print out 4 by 5+31/32" pictures (not 4 by 6). So a border
# needs to be added to make the resolution correct.

# parameters: width height infile outfile
# TODO: Fix the aspect ratio. For now, we produce a 35x45mm image
width=$1
height=$2
infile=$3
outfile=$4

# XRes=width/35 ppmm, YRes=height/45 ppmm.
# XPad=(6in-14cm)/2 = 6.2*XRes
# YPad=(4in-9cm)/2 = 5.8*YRes
xpad=$((31*width/175))
ypad=$((29*height/225))

echo "Final size $((4*width + 2*xpad))x$((2*height + 2*ypad)) (padded by $((2*xpad)), $((2*ypad)))"
montage -mode concatenate -tile 2x2 $infile $infile $infile $infile $infile $infile $infile $infile bmp:- | convert -border ${xpad}x${ypad} -bordercolor '#808080' bmp:- $outfile
