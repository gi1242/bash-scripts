#! /bin/bash

mrxvt --useFifo -hold 4 -tt messages -e tail -f /var/log/messages & pid=$!

# 2007-03-08: Possible race condition if we wait using inotify-wait
i=0
while [[ ! -e /tmp/.mrxvt-$pid ]]; do
    sleep .2
    if ((i++ > 20 )); then
	echo "Timed out while waiting for fifo to be created."
	exit 1
    fi
done

#echo "Waited $i * .2 seconds for fifo to be created"

# Fifo created, echo commands
echo 'NewTab "shorewall" tail -f /var/log/shorewall' >> /tmp/.mrxvt-$pid
echo 'NewTab "cron" tail -f /var/log/cron'	     >> /tmp/.mrxvt-$pid
echo 'NewTab "ntpd" tail -f /var/log/ntpd'	     >> /tmp/.mrxvt-$pid
echo 'UseFifo 0'				     >> /tmp/.mrxvt-$pid
