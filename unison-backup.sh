#! /bin/bash
# Created   : Wed 13 Jun 2012 01:09:54 PM EDT
# Modified  : Wed 22 May 2019 05:20:34 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Backup with unison quietly
if [[ -z "$DISPLAY" ]]; then
    export DISPLAY=:0
fi

msgfile=~/.aosd-msgs
lockfile=/tmp/unison-backup

notify-send "Unison Disabled"
exit 1

if ! pgrep -u gautam unison > /dev/null 2>&1 ||
    ! lockfile-create -p -r0 -l $lockfile
then
    logger "Unison already running. Exiting."
    notify-send "Unison backup failed" "Unison already running"
    exit 1;
fi

if [[ $1 == -c ]]; then
    # Check internet connection quality before backing up.
    checkurl="http://www.math.cmu.edu/~gautam/cgi-bin/connection-check"
    connection=$(ping -q -c 1 -W 1 zxc32.math.cmu.edu > /dev/null \
	&& wget -q -t 1 -T .5 $checkurl -O -)
    cstatus=$?

    if [[ $cstatus == 0 && $connection != OK ]]; then
	notify-send "$(date '+%F %T') Sign-in Required" \
	    "The connected network may require a web-sign in."
	lockfile-remove -l $lockfile
	exit 1
    elif (( $cstatus != 0 )); then
	notify-send "$(date '+%F %T') Warning" \
	    "Network is slow, or servers are unreachable."
	lockfile-remove -l $lockfile
	exit 1
    fi
fi
     
[[ "$UNISON_BACKUP_VERBOSE" == 1 ]] && \
    logger "${HOST:=`hostname`}: Synchronizing with zxc32..."
date "+%F %T ${HOST:=`hostname`}: Synchronizing with zxc32..." >> $msgfile

which keychain >& /dev/null && \
    eval $(SHELL=/bin/bash keychain --noask -Q --quiet --eval 2>/dev/null)
if ssh-add -l > /dev/null; then
    # Authenticated; try doing a silent backup.
    unison -silent -ui text -batch $UNISON_BACKUP_ARGS zxc32
    status=$?
    if (( $status )); then
	logger "Unison backup error: Unison exited with non-zero status"
	notify-send "Unison backup error" "Unison exited with non-zero status"
    fi
else
    status=1;
    notify-send "Unison backup failed" "Not authenticated!"
    logger "Unison backup failed: Not authenticated!"
    date "+%F %T $HOST: Synchronization failed, not authenticated!" >> $msgfile
fi

lockfile-remove -l $lockfile

if (( $status == 0 )); then
    [[ "$UNISON_BACKUP_VERBOSE" == 1 ]] && \
	logger "$HOST: Synchronized with zxc32."
    date "+%F %T $HOST: Synchronized with zxc32." >> $msgfile
fi

exit $status
