#! /bin/bash
# Created   : Sat 13 Jun 2009 04:23:35 PM PDT
# Modified  : Sat 13 Jun 2009 05:21:38 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

files=("$@")
mkdir -p montage

if (( ${#files[@]} / 4 * 4 != ${#files[@]} )); then
    echo "# files (${#files[@]}) is not a multiple of 4."
    exit 1;
fi

for (( i=0; i<${#files[@]}; i=i+4 )); do
    printf -v outfile "montage/%02d.jpg" $((i/4))
    montage -verbose -mode concatenate -tile 2x2 \
	${files[i]} ${files[i+1]} ${files[i+2]} ${files[i+3]} \
	-quality 95 $outfile
done
