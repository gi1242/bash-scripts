#! /bin/bash
# Rsync a minimal set of 'source' files.
#
# Crontab for rsync-minimal:
# 11 * * * * /root/bin/rsync-minimal.sh /backup/hourly/home1
# 41 * * * * /root/bin/rsync-minimal.sh /backup/hourly/home2

# Default to /free/home
rsync_dir=${1:-"/backup/hourly/home"}

# Rsync source files on /free/$1
nice rsync -a --delete --delete-excluded --exclude-from - \
    /home/ $rsync_dir <<- EOF
	# Named directories to backup
	+ src/
	+ math/
	- /*/*

	# Exclude Audio / video
	- *.mp3
	- *.avi
	- *.mov
	- *.wmv
	- *.wav

	# Images
	- *.jpg
	- *.JPG
	- *.png
	- *.gif

	# Program outputs
	- *.pdf
	- *.log
	- *.dvi
	- *.aux
	- *.swp
	- *.o

	# Archives
	*.tar
	*.tar.*
	*.tgz
	*.tbz2
	*.zip
	*.gz
	*.bz2

	# Backups
	- *.bak
	- *.orig*
	- *~
EOF
