#! /bin/bash
# Created   : Sat 10 May 2008 02:31:05 AM PDT
# Modified  : Mon 29 Jun 2009 10:32:58 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Runs ttcp locally, and remotely via ssh
# Usage: ttcpssh host <local args> -- <remote args>

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

declare -a local_args
declare -a remote_args

if (( ! $# )); then
    echo "Usage: $0 <host> <local_args> -- <remote_args>"
    exit 1
fi

host=$1
shift
if [[ $host == *:* ]]; then
    port=${host##*:}
    host=${host%:*}
else
    port=5010
fi

while (($#)); do
    if [[ $1 == "--" ]]; then
	shift;
	break;
    else
	local_args+=("$1")
	shift;
    fi
done
remote_args=("$@")

echo "${UL}($host) ttcp -p$port ${remote_args[@]}${RE}" >> /dev/stderr
ssh -nf $host ttcp -p$port "${remote_args[@]}"
sleep 1

echo "${BD}(localhost) ttcp -p$port ${local_args[@]}${RE}" >> /dev/stderr
ttcp -p$port "${local_args[@]}" $host 
sleep 1
