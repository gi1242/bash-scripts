#! /bin/bash
# Created   : Sat 23 May 2009 10:27:10 PM PDT
# Modified  : Sat 23 Jul 2016 08:23:54 PM EDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

function die()
{
    echo "$*" >> /dev/stderr
    cleanup

    exit 1
}

function cleanup()
{
    if [[ -n "$tmpMnt" && -d "$tmpMnt" ]]; then
	fusermount -u $tmpMnt
	rmdir $tmpMnt
    fi
}

hostname=${1:-root@lembas}
rBackupDir=${2:-/mnt/raid/backup/mordor}

# Get ssh keys added
eval $(sudo -u gautam env SHELL=/bin/bash \
	    keychain --noask -Q --quiet --eval 2>/dev/null)

tmpMnt=$(mktemp -d /tmp/XXXXXXXX)
sshfs $hostname:$rBackupDir $tmpMnt || die "Could not tmp mount $rBackupDir"

logger "Backing up to ${hostname}:${rBackupDir}..."

#
# Take weekly snapshot, if it has not yet been taken
#
curSnapName=$(date "+%Y-week%W")
snapshots=($(find $tmpMnt -maxdepth 1 -mindepth 1 \
    -type d -not -name current -printf '%f\n' | sort))
if (( ${#snapshots[*]} >= 1 )); then
    lastSnap=${snapshots[${#snapshots[*]}-1]}
else
    lastSnap=
fi

#echo "Current: $curSnapName.  Last: $lastSnap. All: ${snapshots[*]}." 1>&2
if [[ "$curSnapName" != ${lastSnap} && -d $tmpMnt/current ]]; then
    echo -n "Taking snapshot $curSnapName..."
    if ssh $hostname cp -al $rBackupDir/current $rBackupDir/$curSnapName;
    then
	echo "OK."
	#echo "OK. Disk usage:"
	#ssh $hostname du -sh $rBackupDir/$lastSnap $rBackupDir/$curSnapName
    else
	echo "failed."
    fi
fi

mkdir -p $tmpMnt/current

if (( ${#snapshots[*]} > 1040 )); then
    echo -n "Removing snapshot ${snapshots[0]}..."
    [[ -n "${snapshots[0]}" && -d $tmpMnt/${snapshots[0]} ]] && \
	rm -rf $tmpMnt/${snapshots[0]} || \
	die "Failed"
    echo "OK"
fi

#
# Backup
#
rsyncArgs="--delete --delete-excluded -av -xSAHX"

echo "Backing up /home:"
rsync $rsyncArgs --exclude-from - \
    /home/ $hostname:$rBackupDir/current/home/ <<- EOF
	+ /gautam
	- /*
	- /*/tmp/
	- /*/ptmp/
	- .adobe/
	- .cache/
	- .ccache/
	- .dbus/session-bus/
	- .gconfd/saved_state
	- .gkrellm2/data/
	- .java/
	- .kde/share/apps/okular/docdata/
	- .keychain/
	- .local/
	- .macromedia/
	- .mozilla/
	- .muttcache/
	- .namazu/
	- .nmzmail/
	- .thumbnails/
	- .thunderbird/
	- .wine/
	- Trash/
	- backup/current/
	- backup/old/
	- cache/
	- etc/anacron-spool/
	- etc/backup_state/
	- .config/gtk-2.0/gtkfilechooser.ini
	- .unison/ar*
	- .xdvirc
	- .lesshist
	- crypt/
	- *.vdi
	- *.vhd
	- *.exe
	- *.rar
	- .*.swp
	- *~
	- *.o
	- *.pdf.xml
EOF

echo "Backing up /root"
rsync $rsyncArgs --exclude-from - \
    /root/ $hostname:$rBackupDir/current/root/ <<- EOF
	- .ccache/
	- .*.swp
	- *~
	- *.o
EOF

echo "Backing up /etc"
rsync $rsyncArgs --exclude-from - \
    /etc/ $hostname:$rBackupDir/current/etc/ <<- EOF
	- /network/run/ifstate
	- /adjtime
	- /mtab
	- .*.swp
	- *~
EOF

cleanup
echo "Completed."
logger "Backup to ${hostname}:${rBackupDir} complete."
