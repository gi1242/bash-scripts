#! /bin/zsh
# Created   : Tue 01 Apr 2014 08:36:10 PM EDT
# Modified  : Mon 18 Feb 2019 03:51:56 PM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Check all html files

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

files=($(find ${1:-~/sj/out} -name \*.html -print))
#ls -d ${repos[@]}

tmpfile=$(mktemp)
for i in $files; do
    if ! tidy -qe $i 2> $tmpfile; then
	echo "${IT}$i${RE}"
	cat $tmpfile
    fi
done

rm -f $tmpfile
