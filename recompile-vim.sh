#! /bin/bash
# Download patches if necessary and recompile vim

function die() {
    echo $* >> /dev/stderr
    exit 1
}

export CFLAGS="-O3 -march=k6-2 -pipe -m3dnow -mmmx -fomit-frame-pointer"
export LDFLAGS="-Wl,-O1 -Wl,--sort-common -s"

if [[ -n $(pgrep pppd) && $1 != "-n" ]]; then
    # We're online. Let's get new patches
    rsync -avzcP --delete --exclude="dos" ftp.nluug.nl::Vim/patches/6.3/ /opt/src/vimpatches/
fi

# Verify integrity of patches
cd /opt/src/vimpatches/
md5sum --status -c < MD5 || die "md5sum of patches failed"

cd ..
rm -rf vim63
tar -xjv < vim-6.3.tar.bz2

cd vim63

# Apply patches
cat ../vimpatches/6.3.* | patch -p0 -f

# Configure and make console version
./configure --prefix=/opt/pkgs/vim63 --disable-gui --without-x --disable-xsmp && make && cp src/vim /opt/pkgs/vim63/bin/vim

# Configure and make gui version
./configure --prefix=/opt/pkgs/vim63 --with-features=huge --enable-perlinterp --enable-pythoninterp && make && cp src/vim /opt/pkgs/vim63/bin/gvim

