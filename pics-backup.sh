#! /bin/bash
# Created   : Tue 21 Apr 2009 08:06:44 PM PDT
# Modified  : Tue 21 Apr 2009 08:10:18 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

year=$(date +%Y)

picsDir=/free/pics
mountPoint=/mnt/andurin-cifs
andurinPicsDir=pics

if [[ ! -d $mountPoint/$andurinPicsDir ]]; then
    sudo mount $mountPoint || (
	echo "Could not mount $mountPoint"
	exit 1;
    )

    mounted=1
fi

rsync -vrx --size-only --delete --delete-excluded \
	$picsDir/$year/ $mountPoint/$andurinPicsDir/$year/

if [[ -n "$mounted" ]]; then
    sudo umount $mountPoint
fi
