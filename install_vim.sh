#! /bin/bash
# Author	: Gautam Iyer <gautam@math.uchicago.edu>
# Created	: Tue 28 Mar 2006 11:23:06 PM CST
# Modified	: Sat 28 Jul 2007 12:04:16 PM EDT
# Description	: Configure and install Vim 7. Run in the source directory.

export CFLAGS=${CFLAGS:--O3 -march=athlon -pipe -fomit-frame-pointer}
export CXXFLAGS=${CXXFLAGS:-$CFLAGS}
export LDFLAGS=${LDFLAGS:--Wl,-O1 -Wl,--sort-common -s}

# Bring source files up to date. Replace locally modified ones by clean ones
# from the CVS repository.
vim_update=1
vim_install=1

while (( $# )); do
    case $1 in
	--noupdate)	vim_update=0;	shift;;
	--noinstall)	vim_install=0;	shift;;

	*)		break;
    esac
done

if (( $vim_update )); then
    if [[ -d CVS ]]; then
	cvs -z9 update -C || exit

	# Get rid of all the backups CVS makes of locally modified files.
	find . -name ".#*" -exec rm -f {} \;
    elif [[ -d .svn ]]; then
	svn revert -R . || exit
	svn update || exit
    else
	echo "No CVS / SVN info found"
	exit
    fi
fi

prefix=${1:-/usr/local/pkgs/vim7}
common_options="--prefix=$prefix --enable-multibyte"

if pgrep -l ^vim; then
    echo "Vim is running. Install will fail"
    exit 1
fi

# Verbosely exec the remaining commands
set -v

# Configure and install the GUI version
./configure $common_options --enable-netbeans || exit 1
if (( $vim_install )); then
    make install || exit 1
else
    make || exit 1
    cp src/vim ${prefix}/bin/vim || exit 1
fi

# Configure and make console version
./configure $common_options --disable-gui --with-x --disable-xsmp || exit 1
make || exit 1

# Make gview etc symlinks point to gvim.
pushd ${prefix}/bin || exit 1
mv -vf vim gvim || exit 1
for i in evim eview gview gvimdiff rgview rgvim; do
    ln -sf gvim $i || exit 1
done

# Copy Vim console binary from src directory to package directory.
popd
cp -v src/vim ${prefix}/bin || exit 1
