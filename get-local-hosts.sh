#! /bin/bash
# Created   : Mon 02 Jun 2008 07:52:40 PM PDT
# Modified  : Mon 02 Jun 2008 08:08:41 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Get local hosts from avahi, and put them in /tmp/local-hosts.csh

network=${1:-eth0;IPv4}
avahi-browse -vtlrkp _ssh._tcp | egrep "^=;$network;" | \
    awk --field-separator=\; -- '{printf "set %s_ip=%s\n", $4, $8}' | \
    tee /tmp/local-hosts.csh
