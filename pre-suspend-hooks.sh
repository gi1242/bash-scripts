#! /bin/bash
# Created   : Sat 25 May 2019 10:24:29 AM EDT
# Modified  : Sat 25 May 2019 11:39:31 AM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

# Just guess; should be good enough for most cases...
export DISPLAY=${DISPLAY:-:0}
export USER=${USER:-gautam}
export HOME=${HOME:-/home/$USER}

# Unmount NSF shares
UMOUNT_FSTYPES='(cifs|smbfs|nfs4|fuse.sshfs)'
mountPoints=($(awk "{if (\$3 ~ /$UMOUNT_FSTYPES/) print \$2}" \
    < /proc/mounts | sort -r))

for d in "${mountPoints[@]}"; do
    d=$(echo -e "$d")
    umount "$d"
done

# Unmount encrypted file systems
if [[ -f /dev/shm/ecryptfs-$USER-Private ]] && \
    (( $(</dev/shm/ecryptfs-$USER-Private) > 1 ));
then
    echo 1 > /dev/shm/ecryptfs-$USER-Private
fi
ecryptfs-umount-private

grep -q ~/crypt /proc/mounts && fusermount -u ~/crypt

# Flush GPG keys
pkill -SIGHUP gpg-agent

true
