#! /bin/bash
# Created   : Wed 09 Aug 2006 04:08:51 PM PDT
# Modified  : Sat 18 Aug 2007 01:34:06 AM CDT
# Author    : Gautam Iyer <gautam@math.stanford.edu>
#
# Delete files that are more than a week old from specified directories
# The specified directories must exist. Defaults to /tmp when run without any
# arguments.

function printHelp()
{
    echo "\
${BD}cleantmp.sh$RE [${IT}dirs$RE]

Deletes all files in dirs which have not been modified for 7 days. If ${IT}dirs$RE is
not specified, use $IT/tmp$RE.

${BD}OPTIONS$RE

    ${UL}-d$RE ${IT}N$RE	Delete files that are ${IT}N$RE days old (instead of 7).
    ${UL}-h$RE		Print this help and exit.
    ${UL}-N$RE		Dry run. Don't actually delete anything.
"
    exit
}

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

# Default option values
declare -a DIRS
daysOld=7
delete="-delete"

while getopts "d:Ehl:N" OPT; do
    case $OPT in
	"d") daysOld=$OPTARG;;
	"h") printHelp;;
	"N") delete="";;	# Dry run. Don't delete
	"?") exit;;
    esac
done

shift $((OPTIND-1))

if (( $# )); then
    DIRS=($@)
else
    DIRS=(/tmp)
fi

# Disable globing
set -o noglob

#exclude_dirs=(.X11-unix .ICE-unix .wine-*)
#exclude_files=(
#    /tmp/.X[0-9]-lock /tmp/.X11-unix/X[0-9] /tmp/krb5cc_*
#    /tmp/dtach-* /tmp/wpa_ctrl_*
#)
#
#prune_args=
#for i in "${exclude_dirs[@]}"; do
#    prune_args="$prune_args -name $i -prune -o"
#done
#
#skip_files=
#for i in "${exclude_files[@]}"; do
#    skip_files="$skip_files ! -wholename $i"
#done

touch_list=(
    /tmp/.X11-unix /tmp/.ICE-unix /tmp/.wine-*
    /tmp/.X[0-9]-lock /tmp/.X11-unix/X[0-9] /tmp/krb5cc_*
    /tmp/dtach-* /tmp/wpa_ctrl_*
)

touch_files=""
for i in "${touch_list[@]}"; do
    touch_files="$touch_files -wholename $i -o"
done
touch_files=${touch_files%-o}
[[ -n $touch_files ]] && touch_files="( $touch_files )"

# If we're not running as root, then only find files owned by user.
uid=$(id -u)
user_args=
if (( $uid != 0 )); then
    prune_args="$prune_args -not -uid $uid -prune -o"
    user_args="( -uid $uid ( -type d -o -type f ) )"
fi

#echo "$0 running as uid $uid"

# Debugging info
if [[ -z $delete ]]; then
    [[ -n $touch_files ]] && echo "${UL}Touching files${RE}"	\
	$(find ${DIRS[@]} -mindepth 1 ${user_args} ${touch_files}   \
	    -ctime +$daysOld -print)

    cat <<- EOF
	Executing ${UL} find ${DIRS[@]} -mindepth 1 $user_args -ctime +$daysOld \( -type f -o -empty \) $delete
	EOF
fi

# Touch important files
[[ -n ${delete} && -n ${touch_files} ]] &&
    find ${DIRS[@]} -mindepth 1 ${user_args} ${touch_files} -ctime +$daysOld \
	-exec touch {} \;

# Delete everything else
find ${DIRS[@]} -mindepth 1 $user_args -ctime +$daysOld			\
    \( ! -type d -o -empty \) $delete -print0 |				\
    xargs -0r echo -e $(date "+\033[32m%F %H:%M:%S:\033[m Removing ")

#find ${DIRS[@]} -mindepth 1 $prune_args $skip_files $user_args		    \
#    -ctime +$daysOld \( -type f -o -empty \) $delete -print0 |	    \
#    xargs -0r echo -e $(date "+\033[32m%F %H:%M:%S:\033[m Removing ")

# Delete empty directories
# Note: All empty directories are deleted, not just old ones. This is because
# once we delete a directory which makes the parent directory empty, the parent
# will not be deleted (as it is no longer old).
#if (( $deleteEmpty )); then
#    find ${DIRS[@]} -mindepth 1 $prune_args $skip_files $user_args	    \
#	-type d -empty $delete -print0 |				    \
#	xargs -0r echo $(date "+%F: Removing empty directories ")
#fi
