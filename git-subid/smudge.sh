#! /bin/sh
# Created   : Thu 19 May 2016 03:33:06 PM EDT
# Modified  : Fri 20 May 2016 11:06:26 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

#rep=$(git log --pretty=format:"%h %ci %s" -1)
rep=$(git rev-parse @)
exec sed -re 's/\$Id(:[^$]+)?\$/$Id: '"$rep"' $/g'
