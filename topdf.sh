#! /bin/bash
# Created   : Fri 12 Sep 2008 05:50:36 PM PDT
# Modified  : Fri 12 Sep 2008 05:52:45 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Convert files to pdf.

input=${1##*/}
dir=${1%/*}
ext=${1##*.}

[[ -n "$dir" ]] && cd $dir

case $ext in
    dvi)
	dvipdf $input
	;;

    *)
	echo "Unrecognized type '$ext'"
	exit 1;
esac
