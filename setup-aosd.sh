#! /bin/bash
# Created   : Thu 14 Aug 2014 09:36:11 PM BRT
# Modified  : Fri 11 Dec 2020 12:14:20 PM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Set up so that anything written to ~/.aosd-msgs gets piped to aosd_cat

force=0
verbose=0
while getopts "fv" OPT; do
    case $OPT in
	"f") force=1;;
	"v") verbose=1;;
    esac
done

msgfile=~/.aosd-msgs
AOSD_ARGS=(-p 0 -x 0 -y 0 -t 2 -B '#ffff00' -R '#004000' -b 255 -s 20 \
    -n 'Sans 10' -d 5 -u 3000)

if (( ! force )) && [[ -e $msgfile ]] && fuser -s $msgfile; then
    (( verbose )) && echo "Already running. Use -f to restart."
    exit 0
fi

# Flush msg file
echo -n > $msgfile
fuser -sk $msgfile && \
    ((verbose)) && echo "Killed previous aosd instance"

tail -n 1 -F $msgfile | aosd_cat "${AOSD_ARGS[@]}" &

(( verbose )) && echo "Setup aosd" >> $msgfile
