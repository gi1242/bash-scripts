#! /bin/bash
# Runs startx, and redirects output to ~/.xsession-errors

function eexec()
{
    echo "$@"
    if (( $dry_run == 0 )); then
	#eval "exec $*"
	#which $1 >& /dev/null && ( exec "$@" & )
	eval "exec $*" &
    fi
}

declare -a client_args server_args

if [[ $1 =~ --dry-?run ]]; then
    shift
    dry_run=1
else
    dry_run=0
fi

if [[ -z $1 || $1 == "--help" ]]; then
    cat << "EOF"
xstart {default|tall|--help|--dry-run}
xstart [startx options]

xstart local, wide, or hometv starts an X server, with settings for local,
department, or tv respectively. --help prints this usage, and --dry-run prints
the command that would be executed.

If the first option is not understood by xstart, it passes all options to
startx. It tires to guess the display number from the command line, and redirect
startx's output to ~/.xsession-errors-N.
EOF
    exit 1
fi

if [[ $1 == default ]]; then
    server_args=(vt7 -dpi 125)
elif [[ $1 == tall ]]; then
    #server_args=(:2 vt8 -layout WideLayout)
    server_args=(:2 vt8 -dpi 125 -layout Tall)
else
    # Get display number (so that output can be appropriately redirected)
    while (($#)); do
	if [[ $1 == "--" ]]; then
	    shift;
	    break;
	else
	    client_args[${#client_args[*]}]=$1
	    shift;
	fi
    done
    server_args=($@)
fi

#
# Figure out the error file based on the display.
#

# If server is given as first argument, skip it.
disp=${server_args[0]}
which $disp >& /dev/null && disp=${server_args[1]}

if [[ $disp == :* ]]; then
    err_file="~/.xsession-errors-${disp#:}"
else
    err_file="~/.xsession-errors"
fi

server_args=(${server_args[@]} -nolisten tcp -br)
eexec "startx ${client_args[@]} -- ${server_args[@]} >& $err_file"
