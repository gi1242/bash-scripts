#! /bin/bash
# Created	: Sat 30 Dec 2006 11:03:47 AM PST
# Modified	: Sat 30 Dec 2006 12:40:14 PM PST
# Author	: Gautam Iyer <gi1242@users.sourceforge.net>
# Description	: Convert ARW files to reasonable image formats


# {{{1 Functions
function die()
{
    echo "${ER}Error:$RE $*" > /dev/stderr
    exit 1
}

function print_help()
{
    echo "\
arwconvert.sh: Convert ARW files to various image formats.

arwconvert.sh [options] [{--|--+} dcraw-options]

    -h		Print this help and exit.
    -o format	Convert to format. Default png.
    -O filter	Pass dcraw output to filter (instead of pnmtopng).
    -e ext	Extension of output files (required with -O).
    -- args	Use args as the arguments to dcraw
    --+	args	Use '$dcrawOpts args' as arguments to dcraw
"
    exit
}
#}}}1

# Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

# Defaults
dcrawOpts="-c -w -v"
oformat="png"
ofilter=""
oext=""
overwrite=0
declare -a files


# Process options
while getopts "O:e:o:fh-" OPT; do
    case $OPT in
	"o") oformat=${OPTARG};;
	"O") ofilter=${OPTARG};;
	"e") oext=${OPTARG};;
	"f") overwrite=1;;
	"h") print_help;;
	"-") break;;
	"?") die "Use $UL-h$RE for help";;
    esac
done

# Get optional extra dcraw args
for (( i=$OPTIND; $i <= $#; i++ )); do
    if [[ ${!i} == "--" ]]; then
	shift $i
	dcrawOpts=$@
	break
    elif [[ ${!i} == "--+" ]]; then
	shift $i
	dcrawOpts="${dcrawOpts} $@"
	break
    else
	files[$(( ${#files[*]}+1 ))]=${!i}
    fi
done

# Sanity check output filter
if [[ -z $ofilter ]]; then
    ofilter="pnmto$oformat"
    oext=$oformat
fi
if ! which "${ofilter%% *}" >& /dev/null; then
    die "Could not find executable ${IT}${ofilter%% *}$RE"
fi
if [[ -z $oext ]]; then die "No output extension specified"; fi

for i in ${files[*]}; do
    ofile=${i%%.arw}.$oext
    ofile=${ofile##*/}

    if [[ $overwrite == 1 || ! -e $ofile ]]; then
	echo "${UL}dcraw $dcrawOpts \"$i\" | $ofilter > \"${i%%.arw}.$oext\"$RE"
	dcraw $dcrawOpts "$i" | $ofilter > "${i%%.arw}.$oext"
    else
	echo "${IT}File $UL$ofile$IT exists, skipping.$RE"
    fi
done
