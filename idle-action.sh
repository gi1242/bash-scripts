#! /bin/bash
# Created   : Thu 07 May 2020 11:51:00 AM EDT
# Modified  : Thu 07 May 2020 01:20:06 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

echo -n "$(date): Starting idle..."
xrefresh

trap 'echo Interrupted; exit 0' TERM INT
trap 'echo Exit' EXIT

sleep 2
xrefresh
