#! /bin/bash
# Created   : Wed 01 Apr 2009 04:18:57 PM PDT
# Modified  : Wed 01 Apr 2009 04:18:57 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Show hit counts of webpages

hit_count_dir=/afs/ir.stanford.edu/users/g/i/gi1242/cgi-bin/hitcount

for i in ${hit_count_dir}/*.counter; do
    name=${i#$hit_count_dir/}
    name=${name%.counter}
    name=${name//_slash_/\/}

    count=$(<$i)

    echo -e "$count\t$name"
done
