#! /bin/bash

function viewman() {
    if [[ -n $DISPLAY ]]; then
	echo -ne "\033]62;man $1\007"
    fi

    while( true); do
	man $1 || exit
    done
}

function xviewman() {
    mrxvt -hold -tt "man $1" -e viewman.sh $1 &> /dev/null &
}

function printhelp() {
    echo "\
viewman.sh -[axh] file

    -a	views all man pages in the current directory (implies -x)
    -x	views in xterm / mrxvt in background
    -h	prints this junk"

    exit
}

while getopts "axh" OPT; do
    case $OPT in
	a)  for i in *.[0-9]; do
		xviewman ./$i
	    done;
	    exit;;

	x)  shift; xviewman $1; exit;;
	?|h)  printhelp;;
    esac
done

viewman $1
