#! /bin/bash
# Usage: get_md5sums.sh <list>
# Computes the md5 sums of all files given on the command line. Directories are
# recursed. Sums are written to stdout.

# Avoid trouble with filenames with spaces
IFS=$'\n'

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[1;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

function die() {
    echo "${ER}$1$RE" >> /dev/stderr
    exit 1
}

function print_help() {
    echo "\
${BD}USAGE:$RE	
    getmd5sums $UL-h$RE | $UL<file-list>$RE

    $UL-h$RE  Print this help

    Computes md5sums of all files given on the command line. Directories are
    recursed. Sums are written to stdout." >> /dev/stderr

    exit
}

declare -a filelist

while getopts "h" OPT; do
    case $OPT in
	h|?) print_help;;
    esac
done

if (( $# == 0 )); then
    print_help;
elif [[ $# == 1 && -d $1 ]]; then
    # Only one directory. Strip directory prefix
    cd $1 || die "Could not enter directory $1"
    filelist=$(find . -type f | cut -c 3-)
else
    filelist=$(find $@ -type f)
fi

for file in ${filelist[@]}; do
    md5sum $file
done
