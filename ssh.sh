#! /bin/bash
# Created   : Sun 20 Apr 2008 01:19:43 PM PDT
# Modified  : Mon 01 Aug 2016 11:13:04 PM EDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Wrapper script for ssh / scp. If you're not yet authenticated, then it runs
# ssh-add to add your keys. Otherwise it blindly calls ssh/scp.
#
# Install instructions: Create symlinks in your path called "ssh" and "scp"
# pointing to this script. Make sure that the directory you create these links
# in appears *before* the directory with the ssh/scp binaries in your path.
#   

#SSH_ADD_ARGUMENTS=~/.ssh/*id_[rd]sa

realprog=$(readlink -f $0)
progname=${0##*/}

if [[ -n "$SSH_AUTH_SOCK" && $1 != -V ]] && ! ssh-add -L >& /dev/null; then
    tty -s && echo -n "(ssh-add) "
    if ! ssh-add ${SSH_ADD_ARGUMENTS}; then
	echo "Failed executing ssh-add" >> /dev/stderr
	exit 1
    fi
fi

# Run ssh/scp from global path.
for prog in $(which -a $progname); do
    if [[ $(readlink -f $prog) != $realprog ]]; then
	exec $prog "$@"
	break; # Should never be reached
    fi
done

echo "Could not find a different instance of $progname in PATH"
