#! /bin/bash

# Start exim / fetchmail
# sudo /etc/init.d/exim start

STATEFILE="/var/state/rsync-deepthought"
LOGFILE="/var/log/connect.log"

cleanup() {
    # trap 'die' SIGHUP SIGINT SIGTERM
    trap - SIGHUP SIGINT SIGTERM
    echo "Recieved signal, exiting"

    # Kill wvdial
    if [[ -n $WVPID && -d /proc/$WVPID ]]; then sudo kill $WVPID; fi

    # Stop running instance of fetchmail
    if [[ -n $(pgrep fetchmail) ]]; then fetchmail -q; fi

    # If xrootconsole is running, kill it after a minute
    if [[ -n $xrootpid ]]; then
	echo "Killing $xrootpid"
	sleep 60
	kill $xrootpid
    fi

    # Terminate the script
    exit
}

connect() {
    if [[ $nomail == 1 ]]; then
	# Just connect.
	sudo wvdial
    else
	# Trap interupts.
	trap 'cleanup' SIGHUP SIGINT SIGTERM

	# Connect and start fetchmail after pppd is up
	sudo wvdial & WVPID=$!

	# Let's wait till pppd starts:
	until [[ -n $(pgrep pppd) || ! -d /proc/$WVPID ]]; do
	    sleep 5
	done

	# Oops did not connect
	if [[ ! -d /proc/$WVPID ]]; then cleanup; fi

	# Give it another 10 seconds to set the IP etc
	sleep 10
	pgrep exim > /dev/null || echo "WARNING: exim not started"
	fetchmail

	# Backup source files to math if not done in the past 24 hours
	# if [[ ! -e $STATEFILE ]]; then
	#     echo "WARNING: no rsync state file found. Not backing up"
	# elif (( $(date +%s) - $(stat -c "%Y" $STATEFILE) > 75600 )); then
	#     echo "Backing up source files..."
	#     ~/bin/rsync-minimal.sh -d gautam@deepthought:local/backup/ --+ -vz
	#     touch $STATEFILE
	# fi

	# On seconds thoughts, let's always backup source files
	~/bin/rsync-minimal.sh -d gautam@deepthought:local/backup/ --+ -vz

	# Let's wait till wvdial is done:
	wait $WVPID
	cleanup
    fi
}

# Check if we're already connected
if (( $(pgrep pppd) )); then
    echo "pppd is already running. You're probably online already."
    exit 1
fi

nomail=0
rootwin=0

while getopts ":nr" OPT; do
    case $OPT in
	n)	nomail=1;;
	r)	rootwin=1;;
	?)	echo "Option $OPTARG undefined."; exit 1;;
    esac
done

if [[ -n $DISPLAY && $rootwin == 1 ]]; then
    # Clobber logfile
    echo -n > $logfile
    xrootconsole --wrap -geometry 80x10+0+132 -fg "#00a000" $LOGFILE &
    xrootpid=$!

    connect > $LOGFILE 2>&1 &
else
    connect 2>&1 | tee $LOGFILE
fi
