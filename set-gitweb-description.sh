#! /bin/bash
# Created   : Fri 27 Jun 2014 10:38:02 AM EDT
# Modified  : Fri 27 Jun 2014 01:14:35 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Set gitweb.description correctly for all projects

for i in ~git/repositories/papers/*.git; do
    cd $i

    for opt in description category cloneurl; do
	if git cat-file -e HEAD:.gitweb/$opt 2>/dev/null; then
	    #git cat-file blob HEAD:.gitweb/$opt  > $GIT_DIR/$opt
	    value=$(git cat-file blob HEAD:.gitweb/$opt)
	    git config --replace-all gitweb.$opt "$value"
	    echo "$value" > $opt
	else
	    rm -f $opt
	fi
    done
done
