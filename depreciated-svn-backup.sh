#! /bin/bash
# Created   : Sun 16 Sep 2012 03:20:57 PM EDT
# Modified  : Sun 16 Sep 2012 04:27:17 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Backup the SVN repository

function die()
{
    echo "$*" >> /dev/stderr

    exit 1
}

[[ -e /tmp/svn-backup.lock ]] && \
    die "Found lockfile /tmp/svn-backup.lock. Quitting."
touch /tmp/svn-backup.lock

svnroot=~/.svnroot
backupDir=/afs/andrew.cmu.edu/usr18/gi1242/backup/svnroot

mkdir -p $backupDir


# Get a list of repos
repos=($(find $svnroot -wholename '*/db/current' -printf '%P\n'))
repos=(${repos[@]%/db/current})

repos=(www-share shared/test shared/2011-leger-pego)

for i in ${repos[@]}; do
    # Following the svn-fast-backup script
    rsync -avR $svnroot/./$i/db/current $backupDir/
    rsync -avR --delete --chmod=Dg-s --exclude db/current --exclude 'db/transactions/*' \
	--exclude 'db/log.*' $svnroot/./$i $backupDir/
done

rm /tmp/svn-backup.lock
