#! /bin/bash
# Created   : Sat 17 Dec 2016 10:57:10 PM EST
# Modified  : Sun 18 Dec 2016 04:53:58 PM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

verbose=0

interval=300
last_backup=0
poll_interval=10

sleep_interval=$poll_interval
while true; do
    cur_time=$(date +%s)
    if (( cur_time - last_backup > interval )); then
	if ~/src/bash/unison-backup.sh; then
	    last_backup=$cur_time
	    sleep_interval=$poll_interval
	else
	    sleep_interval=$((2 * sleep_interval))
	    (( sleep_interval > interval )) && sleep_interval=$interval
	    (( verbose )) && date '+%F %T: Unison backup failed.'
	fi
    fi

    sleep $sleep_interval
done
