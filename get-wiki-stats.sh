#! /bin/zsh
# Created   : Sat 22 Feb 2014 10:51:17 AM EST
# Modified  : Mon 24 Mar 2014 04:41:32 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Get web statistics from wiki

tmpdir=~/tmp/wiki

mkdir -p $tmpdir/{logs,stats,ssl-stats}

# Get logs
rsync --delete -av wiki:/var/log/apache2/ $tmpdir/logs

# http stats
cd $tmpdir/stats
webdruid $tmpdir/logs/access.log*(On,n)
firefox $tmpdir/stats/index.html &

cd $tmpdir/ssl-stats
webdruid $tmpdir/logs/ssl_access.log*(On,n)

firefox $tmpdir/ssl-stats/index.html &
