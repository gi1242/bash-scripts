#! /bin/bash
# Generate 4up booklets. Bottoms have to be cut and interleaved with the tops
# pstops "4:0R@.647(0cm,11in)+2R@.647(0in,5.5in),1L@.647(8.5in,5.5in)+3L@.647(8.5in,0in)"

if [[ $1 =~ ^- ]]; then
    echo "\
Filter to 4up already 2upd booklets. For example use

    dvips -o - apu.dvi | psbook | psup.pl | ps4upbook.sh
"
    exit
fi


# Generate 4up booklets. Cut the bottoms, turn upside down, put below tops.
# Uncentered pages
# psbook | pstops -d "4:1R@.647(0cm,11in)+0L@.647(8.5in,0in),2L@.647(8.5in,5.5in)+3R@.647(0in,5.5in)"

# Cenetered pages
psbook | pstops "4:1R@.647(.691in,11in)+0L@.647(7.809in,0in),2L@.647(7.809in,5.5in)+3R@.647(.691in,5.5in)"
