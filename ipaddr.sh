#! /bin/bash

ipaddr=$(hostname -I)

if [[ $1 == -q ]]; then
    echo $ipaddr
else
    #echo "IP: $ipaddr (external $(wget -qO- http://whatismyip.org))"
    echo "IP: $ipaddr (external $(ssh gi.math.cmu.edu echo '${SSH_CLIENT%% *}'))"
fi
