#! /bin/bash
# Created   : Fri 19 Aug 2011 11:01:21 PM EDT
# Modified  : Fri 15 Aug 2014 12:34:29 AM BRT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# AFS backup if AFS is mounted and authenticated
host=${HOST%%.*}
if [[ -z "$host" ]]; then
    echo "Unknown hostname."
    env
    exit 1
fi

msgfile=~/.aosd-msgs

# Separate multiple directories with a space
afsdirs="/afs/andrew.cmu.edu/usr18/gi1242/backup/$host"

mbScript=$HOME/src/bash/minimal-backup.sh
minWithXojExclude=$HOME/etc/rsync-exclude-lists/minimal-with-xoj
lockdir=$HOME/etc/backup_state

for afsbackuproot in $afsdirs; do
    statusfile=${lockdir}/${afsbackuproot//\//_}

    if [[ $1 == --force ]]; then
	# Remove a log file if it exists. (Respect the existence of a lock file
	# though).
	rm -f $statusfile
    fi

    if [[ ! -e $statusfile && ! -e $statusfile.lock && -w $afsbackuproot ]];
    then
	touch $statusfile.lock

	echo "Backing up to AFS..." >> $msgfile
	if
	    $mbScript -x $minWithXojExclude -d $afsbackuproot/current/ \
		-b $afsbackuproot/old/ --+ --no-group -v > $statusfile.log;

	    # 2008-06-26: SVN comes with a big fat warning that the repository
	    # might get corrupted if it's on a networked file system (even AFS).
	    # Backup the subversion repository just in case.
	    #svnsync sync file://$HOME/backup/svnroot/math \
	    #    >> $statusfile.log;
	then
	    # Clean out files that are more than a month old
	    find $afsbackuproot/old -maxdepth 1 -mindepth 1 -ctime +30  \
		-print -exec rm -rf {} \; >> $statusfile.log
	    mv $statusfile.log $statusfile

	    echo "AFS backup done." >> $msgfile
	else
	    echo "AFS backup error." >> $msgfile
	fi

	rm -f $statusfile.lock
    fi
done
