#! /bin/sh

if [ -z "$1" ]; then
    dir=~/sj/out
else
    dir=$1
    shift
fi

cd $dir
echo "Using $dir as base"
exec python3 -m http.server --cgi --bind 127.0.0.1 "$@"
