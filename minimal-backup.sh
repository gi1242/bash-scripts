#! /bin/bash
# Created   : Unknown (around 2004)
# Modified  : Wed 29 Apr 2015 08:14:26 PM EDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Script to backup important files regularly. Old changes are (incrementally)
# saved indefenately :). Setting up a cron job to delete files in ~/backup/old
# which are older than a week would be a good idea :). cleantmp.sh might help
# there...

function die() {
    echo "$ER: $*$RE" > /dev/stderr
    exit 1
}

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

# Defaults
progName="minimal-backup"
srcDir="$HOME/"
dstDir="$HOME/backup/current/"
backupPrefix="$HOME/backup/old/"
rsyncArgs="-ax --delete --delete-excluded"
excludeFile=""
debug=

export RSYNC_RSH='ssh -o ConnectTimeout=10'

function print_help() {
    echo "\
$BD$progName$RE [${UL}options$RE] {$UL--$RE|$UL--+$RE} [${UL}rsync_args$RE]

${BD}minimal-backup.sh$RE backs up your important source files using rsync. Incremental
changes are stored indefenately. Setting a cron job to delete old changes would
be a good idea.

By default the directories ${BD}src$RE, ${BD}math$RE, ${BD}teaching$RE and ${BD}config$RE are backed up in the
directory ${BD}~/backup/current$RE. Incremental changes are stored in ${BD}~/backup/old$RE.
Files that are unlikely to be source files are excluded (see the exclude list in
the source). You can specify a different exclude list if you like.

${BD}OPTIONS$RE

    $UL-b${RE} ${IT}dir$RE	Directory where incremental changes are stored. Clean this out
		periodically. Default $BD~/backup/old/$RE.
    $UL-l${RE}		Live old backups. Instead of incremental changes, you get a
		full backup. This is done via hardlinks of the current backup,
		so it will not gobble space.
    $UL-d${RE} ${IT}dir$RE	Destination for current backup (include trailing '$UL/$RE'.
		Default $BD~/backup/current/$RE.
    $UL-D${RE}		Debug (dont' use). Use ${UL}--+ -nv$RE or ${UL}--+ -vv$RE if you want rsync to
		give you some info about what's bieng done (see man ${BD}rsync$RE).
    $UL-h${RE}		Print this help
    $UL-s${RE} ${IT}dir$RE	Source directory to be backed up (include trailing slash).
		Default $BD~$RE.
    $UL-x${RE} ${IT}file$RE	File containing list of files to exclude while backing up. The
		format of this is the same as that of rsync exclude files. The
		default contains a large list of files that possibly aren't
		sources. Use ${IT}NONE$RE for no exclude file.
    $UL--${RE} ${IT}args$RE	Specify rsync arguments.
    $UL--+${RE} ${IT}args$RE	Arguements for rsync in addition to the default:
		    ${BD}$rsyncArgs$RE
"
    exit
}

while getopts "b:d:s:x:Dlh-" OPT; do
    case $OPT in
	"x") excludeFile=${OPTARG};;
	"b") backupPrefix=$OPTARG;;
	"s") srcDir=${OPTARG};;
	"d") dstDir=${OPTARG};;
	"l") linkBackups=1;;
	"D") debug=1;;
	"h") print_help;;
	"-") break;;
	"?")
	    echo "Unrecognised option: $OPTARG"
	    print_help;;
    esac
done

# Get optional extra rsync args
for (( i=1; $i < $#; i++ )); do
    if [[ ${!i} == "--" ]]; then
	shift $i
	rsyncArgs="$@"
	break
    elif [[ ${!i} == "--+" ]]; then
	shift $i
	rsyncArgs="${rsyncArgs} $@"
	break
    fi
done

[[ -z $dstDir ]] && die "Error: Empty destination directory"
# We're on a remote machine. Can't use versions :(
[[ $dstDir =~ @|: ]] && backupPrefix=

# Figure out where backups go.
if [[ -z $backupPrefix ]]; then
    # No old versions
    backupOpts=
else
    backupDir="$(date "+$backupPrefix%m%d%H%M")"
    backupOpts="--backup --backup-dir $backupDir"
    mkdir -p $backupDir
fi

if [[ $debug ]]; then
    echo "Prefix:		$DEST_PREFIX"
    echo "Destination:	$dstDir"
    echo "Exclude file:	$excludeFile"
    echo "Source dir:	$srcDir"
    echo "lastRun:	$lastRun"
    echo "Rsync args:	$rsyncArgs"
    echo "backupOpts:	$backupOpts"
fi

echo "${HOST:=`hostname`}: Backing up to ${dstDir%%.*:*}" >> ~/.aosd-msgs
if [[ -n $backupDir && $linkBackups == 1 ]]; then
    cp -rl ${dstDir} ${backupDir}
fi

if [[ $excludeFile == "NONE" ]]; then
    nice rsync ${rsyncArgs} $backupOpts ${srcDir} ${dstDir} 
elif [[ $excludeFile ]]; then
    nice rsync ${rsyncArgs} $backupOpts --exclude-from ${excludeFile} ${srcDir} ${dstDir} 
else
    die "No exclude file"
fi
echo "${HOST:=`hostname`}: Completed backup up to ${dstDir%%.*:*}" \
    >> ~/.aosd-msgs
