#! /bin/bash
# Created   : Sat 01 Sep 2012 03:30:49 PM EDT
# Modified  : Tue 10 Sep 2013 12:07:03 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Backup everything on wiki to project

function die()
{
    echo "$*" >> /dev/stderr
    cleanup

    exit 1
}

function cleanup()
{
    if [[ -n "$tmpMnt" && -d "$tmpMnt" ]]; then
	fusermount -u $tmpMnt
	rmdir $tmpMnt
    fi
}


function fullBackup()
{
    hostname=${1:-gautam@project}
    rBackupDir=${2:-backup-wiki}

    ssh $hostname mkdir -p $rBackupDir \&\& chmod og= $rBackupDir
    tmpMnt=$(mktemp -d /tmp/XXXXXXXX)
    sshfs $hostname:$rBackupDir $tmpMnt || die "Could not tmp mount $rBackupDir"

    date "+%c: Backing up to ${hostname}:${rBackupDir}."

    #
    # Take weekly snapshot, if it has not yet been taken
    #
    curSnapName=$(date "+%Y-week%W")
    snapshots=($(find $tmpMnt -maxdepth 1 -mindepth 1 \
	-type d -not -name current -printf '%f\n' | sort))
    if (( ${#snapshots[*]} >= 1 )); then
	lastSnap=${snapshots[${#snapshots[*]}-1]}
    else
	lastSnap=
    fi

    #echo "Current: $curSnapName.  Last: $lastSnap. All: ${snapshots[*]}." 1>&2
    if [[ "$curSnapName" != ${lastSnap} && -d $tmpMnt/current ]]; then
	echo -n "Taking snapshot $curSnapName..."
	if ssh $hostname cp -al $rBackupDir/current $rBackupDir/$curSnapName;
	then
	    echo "OK."
	    #echo "OK. Disk usage:"
	    #ssh $hostname du -sh $rBackupDir/$lastSnap $rBackupDir/$curSnapName
	else
	    echo "failed."
	fi
    fi

    mkdir -p $tmpMnt/current

    if (( ${#snapshots[*]} > 1040 )); then
	echo -n "Removing snapshot ${snapshots[0]}..."
	[[ -n "${snapshots[0]}" && -d $tmpMnt/${snapshots[0]} ]] && \
	    rm -rf $tmpMnt/${snapshots[0]} || \
	    die "Failed"
	echo "OK"
    fi

    #
    # Backup
    #
    rsyncArgs=(--delete --delete-excluded -av -xSAHX
	--rsync-path='rsync --fake-super')

    rsync "${rsyncArgs[@]}" --exclude-from - \
	/ $hostname:$rBackupDir/current/ <<- EOF
		- /usr
		- /bin
		- /var/cache
		- /var/lib/apt
		- /var/lib/dpkg
		- /var/log
		- tmp/
		- ptmp/
		- *~
	EOF

    cleanup
    date "+%c: Backup to ${hostname}:${rBackupDir} complete."

}

lockdir=$HOME/etc/backup-state
mkdir -p $lockdir

which keychain >& /dev/null && \
    eval $(SHELL=/bin/bash keychain -Q --quiet --eval 2>/dev/null)

if [[ ! -e $lockdir/project-full.lock ]] && \
	ssh -o ConnectTimeout=5 gautam@project true 2>/dev/null;
then
    touch $lockdir/project-full.lock

    if fullBackup gautam@project backup-wiki \
	    >> $lockdir/project-full.log 2>&1
    then
	#mv $lockdir/project-full.log $lockdir/project-full
	rm -f $lockdir/project-full.lock
    else
	echo "project full backup failed!"
	echo "Manually remove $lockdir/project-full.lock to renable."
    fi
else
    echo "Error: project backup already running, or could not connect."
fi
