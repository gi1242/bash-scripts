#! /bin/bash
# Created   : Sat 01 Mar 2014 10:11:29 PM EST
# Modified  : Sun 02 Mar 2014 08:28:19 AM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

# Make sure inputs are tex files
LOCAL="$1"
REMOTE="$2"
MERGED="$3"

if [[ "${MERGED##*.}" == tex ]]; then
    output="${MERGED%.tex}-diff.tex"

    if [[ -f "$output" ]]; then 
	read -p "File $output exists. Overwrite? " confirm
	[[ "$confirm" != y && "$confirm" != yes ]] && exit 1
    fi

    latexdiff "$LOCAL" "$REMOTE" > "$output"
    echo "Generated $output"
else
    echo "Skipped $MERGED (non tex)."
fi
