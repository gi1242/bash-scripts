#! /bin/bash
# Created	: Thu 25 May 2006 01:15:19 AM CDT
# Modified	: Fri 30 Oct 2009 11:13:15 PM EDT
# Author	: Gautam Iyer <gi1242@users.sourceforge.net>
# Description	: Convert man pages to text

function print_usage ()
{
    echo "\
${BD}man2txt.sh${RE} [${UL}-Nhof${RE}] [${IT}manpage$RE [${IT}outfile${RE}]]

Convert a manpage to plain text.

${BD}OPTIONS$RE

    $UL-N$RE	Dry run. Just print the command.
    $UL-h$RE	Print this help and exit.
    $UL-o$RE	If ${IT}outfile$RE is not specified, guess a name for it.
    $UL-f$RE	If a file by the guessed name for ${IT}outfile$RE exists, overwrite it."

    exit 1
}


#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

# Extended pattern matching
shopt -s extglob

# Initialize options
force=0
dryrun=0
writetofile=0

# Process options
while getopts "fhNo" OPT; do
    case $OPT in
	"f")	force=1;;
	"N")	dryrun=1;;
	"o")	writetofile=1;;
	"h")	print_usage;;
	"?")	print_usage;;
    esac
done

shift $((OPTIND-1))

# Get input / output file names
infile=$1
outfile=$2

if [[ -n $infile && ! -e $infile ]]; then
    infile=$(man -W ${infile} | head -n 1)
    [[ -z $infile ]] && exit 1;
fi

if [[ -n $infile && -z $outfile && $writetofile = 1 ]]; then
    outfile=${infile##*/}
    outfile=${outfile%.gz}
    outfile=${outfile%.[0-9]?(?)}

    if [[ $force == 0 && -e $outfile ]]; then
	echo "${ER}Error: ${IT}$outfile$RE already exists. Use ${UL}-f$RE to overwrite"
	exit
    fi
fi

cmd=""
if [[ -n $infile ]]; then
    case ${infile} in
	*.bz2)	cmd="bunzip2 -c ${infile} | ";;
	*.gz)	cmd="gunzip -c ${infile} | ";;
	*)	cmd="cat $infile | ";;
    esac
fi

cmd="${cmd} groff -mman -Tascii -P -cbu"

if [[ -n $outfile ]]; then
    cmd="${cmd} > ${outfile}"
fi

if [[ $dryrun == 1 ]]; then
    echo "${cmd}"
else
    eval ${cmd} || echo "${ER}Error:$RE Failed converting manpage ${IT}${infile:-(stdin)}$RE."
fi
