#! /bin/bash

usage() { echo "Usage: curl https://rclone.org/install.sh | sudo bash [-s beta]" 1>&2; exit 1; }

#check for beta flag
if [ -n "$1" ] && [ "$1" != "beta" ]; then
    usage
fi

if [ -n "$1" ]; then
    install_beta="beta "
fi


# Make sure we don't create a root owned .config/rclone directory #2127
export XDG_CONFIG_HOME=config

#check installed version of rclone to determine if update is necessary
version=`rclone --version 2>>errors | head -n 1`
if [ -z "${install_beta}" ]; then
    current_version=`curl https://downloads.rclone.org/version.txt`
else
    current_version=`curl https://beta.rclone.org/version.txt`
fi

if [ "$version" = "$current_version" ]; then
    printf "\nThe latest ${install_beta}version of rclone ${version} is already installed.\n\n"
    exit 3
fi

#download and unzip
if [ -z "${install_beta}" ]; then
    rclone_pkg="rclone-current-linux-amd64.deb"
    download_link="https://downloads.rclone.org/$rclone_pkg"
else
    rclone_pkg="rclone-beta-latest-$OS-$OS_type.zip"
    download_link="https://beta.rclone.org/$rclone_pkg"
fi

cd /root/dl
curl -O $download_link

dpkg -i $rclone_pkg
apt -f install
