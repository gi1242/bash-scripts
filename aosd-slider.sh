#! /bin/zsh
# Created   : Sat 13 Aug 2016 10:33:58 AM EDT
# Modified  : Sat 13 Aug 2016 12:08:19 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

export LANG=en_US.UTF-8

integer percent

percent=$(( $1 < 100 ? $1 + .5 : 100 ))
text=$2

s='\u25fc'
shaded=$s$s$s$s$s$s$s$s$s$s
shaded=$shaded$shaded$shaded$shaded$shaded
e='\u25fb'
empty=$e$e$e$e$e$e$e$e$e$e
empty=$empty$empty$empty$empty$empty
half='\u25fe'

slider=${shaded[1, (percent/2)*6]}
(( percent % 2 )) && slider+=$half || slider+=$e
slider+=$empty[1,6*(50-percent/2 -1)]

pidfile=/tmp/$USER-aosd-slider.pid
if [[ -e $pidfile ]]; then
    pid=$(<$pidfile)
    [[ -r /proc/$pid/comm && $(</proc/$pid/comm) == aosd_cat ]] && kill $pid
fi
echo $text $slider | aosd_cat -n 'Sans 25' -s 128 -R '#00ff00' -t 0 &
echo $! > $pidfile
