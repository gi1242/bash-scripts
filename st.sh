#! /bin/bash
# Created   : Sun 16 Nov 2014 08:43:31 PM EST
# Modified  : Tue 14 May 2019 08:07:11 AM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
# 
# Push and pull from subtrees.

function die()
{
    echo "$@" >> /dev/stderr
    exit 1
}

function eexec()
{
    echo "Executing $@" >> /dev/stderr
    "$@" || die "Failed running $@"
}

git_root=$(git rev-parse --show-toplevel)
prefix=$(git rev-parse --show-prefix)
prefix=${prefix%/}

[[ -z $git_root ]] && die "Couldn't get git root"

if [[ -z $prefix ]]; then
    [[ $1 != add ]] && die "No prefix specified"
    prefix=$2
    [[ -z $prefix ]] && die "No prefix specified with add"
fi

branch=$(git config --get subtree.$prefix.branch 2>/dev/null)
[[ -z $branch ]] && branch=master

cd $git_root

remotes=($(git config --get-all subtree.$prefix.remote))
case $1 in
    (add)
	eexec git subtree add --prefix $prefix @
	;;

    (pull)
	r=${remotes[0]}
	[[ -z $r ]] && die "No remotes configured for $prefix"
	eexec git subtree $1 --prefix $prefix $r $branch
	;;

    (push)
	for r in "${remotes[@]}"; do
	    eexec git subtree push --prefix $prefix $r $branch
	done
	;;

    (*)
	die "Unknown command $1"
	;;
esac
