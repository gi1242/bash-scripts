#! /bin/bash
# Waits untill processes specified on the cmdline are done

usage() {
    echo "\
pwait {-p pids | pgrep-args}

Waits until the processes specified are done. The -p option takes a list of
pids. Otherwise the arguements are passed to pgrep to obtain a list of pids"

    exit
}

declare -a pid
bell=5

while getopts ":b:hp" OPT; do
    case $OPT in
	"p")	shift $((OPTIND-1)); pid=($@); break;;
	"h")	usage;;
	"b")	bell=$OPTARG;;
	":")	echo "Option $OPTARG requires arguement"; exit;;
	"?")	break;;
    esac
done

# If -p is not given, let's pass the command line to pgrep
if (( !${#pid[@]} && $# )); then
    shift $((OPTIND-1))
    pid=($(pgrep $@))
fi

if (( !${#pid[@]})); then
    echo "No processes to wait for."
    exit
fi

echo "Waiting for processes ${pid[@]}"

# Let's get the pids
for(( i=0; i < ${#pid[@]}; i++)); do
    echo -n "Process	${pid[$i]}	"
    while [[ -e /proc/${pid[$i]} ]]; do
	sleep 15
    done
    date "+done (%T %D)"
done

# Ring a bell
for(( i=0; i<bell; i++)); do
    sleep 1;
    echo -ne '\a';
done
