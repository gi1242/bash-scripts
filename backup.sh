#! /bin/bash
# Do all my backups

shopt -s nocasematch
#set +m # Don't monitor jobs.

function finish()
{
    lockfile-remove -l /tmp/no-suspend
    rm -f $lockdir/backup.lock
    exit $1
}

function remote_backup()
{
    host=$1
    crypt=$2
    # Check to see if a backup is needed and the host is accessible
    #if [[ ! -e $lockdir/$host ]] && hostx -Q $host &>/dev/null; then
    if
	    [[ ! -e $lockdir/$host && ! -e $lockdir/$host.lock ]] &&
	    host $host &> /dev/null;
    then
	# Some hosts are not always online. Test if they are accessible. Note --
	# Ping check is quick. Ssh check is slow.
	# 2008-04-01: Need to ssh into gondor, since if it is asleep, the DNS
	# info is cached by my router for a few hours after it is put to sleep.
	if
	    ! [[ $host =~ ^(banach|gondor) ]] ||
	    (
		ping -c 1 -w 1 $host &>/dev/null &&
		ssh -n -o ConnectTimeout=1 $host true &> /dev/null
	    )
	then
	    touch $lockdir/$host.lock

	    if (( $crypt )); then
		ssh $host 'mkdir -p encbackup; chmod og= encbackup' \
			> $lockdir/$host.log
		$cryptBackupScript ~/backup/current $host:encbackup \
			>> $lockdir/$host.log \
		    && mv $lockdir/$host.log $lockdir/$host
	    else
		# Unencrypted backup
		$mbScript -x $excludeFile -d $host:backup/ --+ \
			--chmod=og= -v > $lockdir/$host.log \
		    && mv $lockdir/$host.log $lockdir/$host
	    fi
	    rm -f $lockdir/$host.lock
	fi
    fi
}

function lembas-backup()
{
    # 2012-06-13 gi1242: Avoid waking lembas for a backup for now. Will figure
    # out a different solution later.
    if [[ ! -e $lockdir/lembas-full && ! -e $lockdir/lembas-full.lock ]] && \
	    host lembas.gi1242.net >& /dev/null && \
	    ssh -o ConnectTimeout=5 lembas true 2>/dev/null;
    then
	touch $lockdir/lembas-full.lock
	if ! ssh -o ConnectTimeout=5 lembas true 2>/dev/null ; then
	    # Need to wake lembas up
	    wakeonlan 00:14:D1:19:13:59 >> $lockdir/lembas-full.log
	    for (( i=0; i<30; i++ )); do
		if ssh -o ConnectTimeout=10 lembas true 2>/dev/null; then
		    haveWoken=1
		    break;
		fi
	    done
	    if [[ $haveWoken != 1 ]]; then
		echo "Could not wake lembas"
		rm -f $lockdir/lembas-full.lock
		return
	    fi
	fi

	echo "Backing up to lembas..." >> $msgfile
	if nice sudo /root/sbin/full-backup root@lembas \
		/mnt/raid/backup/mordor >> $lockdir/lembas-full.log 2>&1;
	then
	    mv $lockdir/lembas-full.log $lockdir/lembas-full
	    rm -f $lockdir/lembas-full.lock
	    echo "Done!" >> $msgfile
	else
	    echo "lembas full backup failed!"
	    echo "Manually remove $lockdir/lembas-full.lock to renable."
	    notify-send "Back up to lembas failed." \
		"Manually remove $lockdir/lembas-full.lock to renable."
	fi

	# 2012-06-13 gi1242: Doing a 3 way sync is problematic. I've enabled
	# syncing modification times; hopefully this will resolve conflicts.
	# Disable for now and revisit it later.
	#unison -silent -ui text -batch lembas >> $lockdir/lembas-full.log 2>&1;

	if [[ $haveWoken == 1 ]]; then
	    echo "Sleeping lembas" >> $lockdir/lembas-full
	    ( ssh root@lembas sleep 5m \; pm-suspend >> $lockdir/lembas-full & )
	    sleep 5
	fi
    fi
}

#{{{ Old backup function on andurin
#function andurin_backup()
#{
#    # Return if we can't find andurin
#    ( [[ -e $lockdir/andurin ]] || ! hostx -Q andurin &>/dev/null ) && return
#
#    # Rsync using NFS (faster than ssh).
#    andurinHome=/misc/andurin/home/gautam
#    picsDir=/free/pics/$(date +%Y)/
#    andurinPicsDir=/misc/andurin/pics/$(date +%Y)/
#
#    rsync -ax --exclude .crypt/cRuFAbfdb64B,- --delete --delete-excluded    \
#	    --no-whole-file $HOME/ $andurinHome/backup/baradur/ || return
#
#    if [[ -d $picsDir ]]; then
#	rsync -ax --no-whole-file --delete --delete-excluded		    \
#	    $picsDir $andurinPicsDir || return
#    fi
#
#    # Successful. Create lock file.
#    touch $lockdir/andurin
#}
#}}}

# 2008-03-30: Lock files get created with perms 0666.
umask 0022

# Stop pm-utils from suspending the computer
lockfile-create -r0 -l /tmp/no-suspend

mbScript=$HOME/src/bash/minimal-backup.sh
cryptBackupScript=$HOME/src/private/encbackup.sh
minExclude=$HOME/etc/rsync-exclude-lists/minimal
minWithXojExclude=$HOME/etc/rsync-exclude-lists/minimal-with-xoj
lockdir=$HOME/etc/backup_state
#afsdirs="/afs/ir.stanford.edu/users/g/i/gi1242 /afs/andrew.cmu.edu/usr18/gi1242"
afsdirs="/afs/andrew.cmu.edu/usr18/gi1242/backup/mordor"
msgfile=~/.aosd-msgs
export DISPLAY=:0


# Test if another backup process is already running.
if [[ -e $lockdir/backup.lock ]]; then
    echo "Backup already running. Remove $lockdir/backup.lock if stale."
    pstree -ap

    notify-send "Backup already running." \
	"Manually remove $lockdir/backup.lock if stale."
    exit 1
    # Don't call finish to exit here, since it will remove the lock file.
fi

touch $lockdir/backup.lock

if [[ ! -e $mbScript ]]; then
    echo "Unable to find $mbScript"
    finish 1
fi

# Create the lock directory if it doesn't exist
[[ ! -e $lockdir ]] && mkdir -p $lockdir

# Delete all state files that are older than one day (1440mins). Lock files are
# preserved
find $lockdir -type f -mmin +1447 -and -not -name '*.lock' -delete

#
# Backup routines start here.
#

# First backup locally.
$mbScript -x $minWithXojExclude

# Network backups; see if we are on a reasonably quick network.
connection=$(wget -q -t 1 -T .5 \
    www.math.cmu.edu/~gautam/cgi-bin/connection-check -O -)
which keychain >& /dev/null && \
    eval $(SHELL=/bin/bash keychain --noask -Q --quiet --eval 2>/dev/null)

if [[ $connection == OK ]]; then
    # AFS backup if needed, and if AFS is mounted and authenticated
    ~/src/bash/afs-backup.sh

    ~/src/bash/unison-backup.sh
fi

# Do the remaining backups only if we're running on AC power.
#egrep -sq '^state: *on-line' /proc/acpi/ac_adapter/*/state || finish 0
if ! acpi -a | egrep -sq 'on-line$'; then
    finish 0
fi

#
# Run daily/weekly jobs via anacron.
#
# NOTE: Anacron doesn't care if these jobs succeed/fail. So this can't be used
# for backups, since if a daily backup fails, one should try backing up again
# ASAP, and not the next day.
# 
# Use this only for noncritical tasks (e.g. cleaning tmp files, etc.)
[[ -r ~/etc/anacrontab.$HOST && -x /usr/sbin/anacron ]] && \
    /usr/sbin/anacron -S ~/etc/anacron-spool-$HOST/ -s -t ~/etc/anacrontab.$HOST

#
# Minimal backup on as many remote servers as possible :).
#
# 2008-04-16: SSH keys are now encrypted. Load the agent config
#[[ -z "$HOST" ]] && HOST=$(uname -n)
#[[ -f ~/.keychain/$HOST-sh ]] && source ~/.keychain/$HOST-sh
if ssh-add -l > /dev/null; then
    # Backup xoj files on fast access computers with large quotas
    excludeFile=$minWithXojExclude
    #remote_backup zxc32.math.cmu.edu
    #remote_backup project.math.cmu.edu
    #remote_backup lecturenotes.math.cmu.edu
    remote_backup gondor.gi1242.net

    # Don't back up xoj files on these computers
    excludeFile=$minExclude
    #remote_backup classes.cs.uchicago.edu
    #remote_backup math.uchicago.edu
    #remote_backup banach.stanford.edu
    #remote_backup cauchy.math.duke.edu
    #remote_backup cayley.math.psu.edu

    # Encrypted backups
    remote_backup qwe2.math.cmu.edu 1
    remote_backup math.stanford.edu 1

    lembas-backup
fi

if [[ ! -e $lockdir/sd && ! -e $lockdir/sd.lock ]]; then
    touch $lockdir/sd.lock
    if nice sudo /root/sbin/sd-backup > $lockdir/sd.log; then
	mv $lockdir/sd.log $lockdir/sd
	rm -f $lockdir/sd.lock
    else
	echo "ZFS backup on SD card failed, and is being disabled."
	echo "Manually remove $lockdir/sd.lock to renable."
	notify-send "SD backup failed, and is being disabled." \
	    "Manually remove $lockdir/sd.lock to reenable."
    fi
fi

finish 0


# {{{ Disabled backup methods below.
# Run a comprehensive backup on andurin
#andurin_backup

# Backup pictures (NAS-FAT32, so no perms preserved).
# Temporarily disable
#if [[ ! -e $lockdir/pics && ! -e $lockdir/pics.lock ]] && \
#    ping -c 1 -w 1 andurin.gi1242.net >& /dev/null
#then
#    touch $lockdir/pics.lock
#
#    year=$(date +%Y)
#    picsDir=/free/pics/$year/
#    andurinPicsDir=/misc/andurin/pics/$year/
#    if [[ -d /misc/andurin/ && -d $picsDir ]]; then
#	rsync -vrx --size-only --delete --delete-excluded \
#	    $picsDir $andurinPicsDir > $lockdir/pics.log && \
#	    mv $lockdir/pics.log $lockdir/pics
#    fi
#
#    rm -f $lockdir/pics.lock
#fi

# Comprehensive backup to ZFS pool on andurin (NAS).
#if [[ ! -e $lockdir/andurin && ! -e $lockdir/andurin.lock ]] && \
#	ping -c 1 -w 1 andurin.gi1242.net >& /dev/null; then
#
#    touch $lockdir/andurin.lock
#    if nice sudo /root/sbin/zfs-backup andurin > $lockdir/andurin.log; then
#	mv $lockdir/andurin.log $lockdir/andurin
#	rm -f $lockdir/andurin.lock
#    else
#	echo "ZFS backup on andurin failed, and is being disabled."
#	echo "Manually remove $lockdir/andurin.lock to renable"
#    fi
#fi

# Comprehensive backup on SD card
#if [[ ! -e $lockdir/sd && ! -e $lockdir/sd.lock ]]; then
#    touch $lockdir/sd.lock
#    if nice sudo /root/sbin/zfs-backup sd > $lockdir/sd.log; then
#	mv $lockdir/sd.log $lockdir/sd
#	rm -f $lockdir/sd.lock
#    else
#	echo "ZFS backup on SD card failed, and is being disabled."
#	echo "Manually remove $lockdir/sd.lock to renable."
#    fi
#fi

# Backup email
#if [[ ! -e $lockdir/mail ]] && hostx -Q math.stanford.edu >& /dev/null	    \
#	&& ! pgrep -f imapd >& /dev/null; then
#    #offlineimap -u Noninteractive.Quiet
#    mbsync -q -a && touch $lockdir/mail || echo "Error synchronizing mail"
#fi
