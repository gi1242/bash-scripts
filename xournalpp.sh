#! /bin/sh
exec flatpak run --branch=stable --arch=x86_64 --command=xournalpp \
	--file-forwarding com.github.xournalpp.xournalpp "$@"
