#! /bin/bash
# Created   : Mon 29 Apr 2019 09:49:59 PM EDT
# Modified  : Mon 29 Apr 2019 10:17:35 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

# Exit if any command fails
set -e

[[ -z $1 ]] \
    && echo "Usage: create-gitweb 'gitweb description'" \
    && false

[[ -z $(git ls-files) ]] \
    && echo "Error: Empty repository" \
    && false

[[ -n $(git status --porcelain) ]] \
    && echo "Uncomitted changes, aborting" \
    && false
    
git checkout --orphan=gitweb
git rm -rf .
echo "$@" > description
git add description
git commit -m 'Added gitweb description'
git checkout master
git worktree add .gitweb gitweb
grep -q '^/\.gitweb/$' .git/info/exclude \
	|| echo /.gitweb/ >> .git/info/exclude
