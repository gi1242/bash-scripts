#! /bin/bash

if [[ -n $DISPLAY && -n $MRXVT_TABTITLE ]]; then
    echo -ne "\e]79;\a" 1>&2
    less
    echo -ne "\e]79;\a" 1>&2
else
    exec less
fi
