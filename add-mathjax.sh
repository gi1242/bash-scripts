#! /bin/bash
# Created   : Thu 12 Mar 2015 09:08:53 PM EDT
# Modified  : Thu 12 Mar 2015 10:44:17 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Add a MathJAX header to the script if needed.

file=$1

if [[ -z $file ]]; then
    echo "No filename given"
    exit 1
fi

# First test if we need it.
if grep -qE '\\[[(]|\$|\\begin\{' $file; then
    # Add it to the end of the head section.
    sed -e '/<\/head>/i	<script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>' -i $file
fi
