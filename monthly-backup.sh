#! /bin/bash

pkgdir="/backup/portage/packages"
tmpdir="/backup/tmp"

# Save previous backup
mv -f $pkgdir $tmpdir

# Make a new snapshot
nice quickpkg $(qpkg -nc -I)
