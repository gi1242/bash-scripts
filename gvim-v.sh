#! /bin/bash
# Created   : Sun 06 Feb 2011 09:28:24 PM EST
# Modified  : Sun 06 Feb 2011 09:33:22 PM EST
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Start gvim if the output is not a terminal. Else start gvim -v.

if tty -s && [[ $(ps h -o comm --pid $PPID) =~ ^((ba)?sh|t?csh|zsh)$ ]]; then
    exec gvim -v "$@"
else
    exec gvim "$@"
fi
