#! /bin/zsh
# Created   : Tue 01 Apr 2014 08:36:10 PM EDT
# Modified  : Sat 25 May 2019 11:05:35 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Check all git repositories

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

dir=${1:-~}
repos=(${$(find $dir -xdev -name tmp -prune -or -name ptmp -prune \
	    -or -name backup -prune -or -name .git -type d -print):h})
#ls -d ${repos[@]}

for i in $repos; do
    name=${i/$HOME/~}
    echo "${UL}Repository ${name}${RE}"
    cd $i
    git fsck

    # Check if no remote is present
    if (( $(git remote -v | wc -l ) == 0 )); then
	echo "${ER}WARNING: Repository $name has no remote.${RE}"
    fi
    cd -
done
