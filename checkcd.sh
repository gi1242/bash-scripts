#! /bin/bash
# checks md5sums of the contents of your cd

# Avoid trouble with filenames with spaces
IFS=$'\n'

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[1;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

function print_help() {
    echo "\
${BD}checkcd.sh${RE} - Checks the md5sums on the CD.

Checks if all files on the CD/DVD have the same md5 sums as in the source
directory. If a file containing md5sums of the source directory is found, then
this is used. If not md5sums of the source directory are recomputed and stored
in this file.

${BD}OPTIONS$RE
    $UL-i ${IT}src-dir$RE	Specify the source directory (required).
    $UL-m ${IT}mnt-pt$RE	CD/DVD mount point ($mountPoint)
    $UL-N$RE		Do not mount/unmount the CD/DVD
    $UL-5 ${IT}md5dir$RE	Directory where md5 sums are stored ($md5dir)
    $UL-r$RE		Recompute md5sum (even if the md5 sum file exists).
    $UL-l ${IT}log-dir$RE	Directory where output log goes (/tmp)
    $UL-h$RE		Print help"

    exit
}

function die() {
    echo "$ER$1$RE"
    exit 1
}

function getmd5() {
    # Takes one directory and computes md5sums of files in that directory
    declare -a filelist

    cd $1 || die "ERROR: Unable to open directory $1"
    filelist=$(find . -type f | cut -c 3-)

    for file in ${filelist[@]}; do
	md5sum $file
    done

    cd -
}

md5dir="/tmp"
srcDir=""
noMount=0
recompute=0
mountPoint="/mnt/cdrom"
logDir="/tmp"

while getopts "i:m:5:l:rNh" OPT; do
    case $OPT in
	i) srcDir=${OPTARG%/};;
	5) md5dir=${OPTARG%/};;
	l) logDir=${OPTARG%/};;
	m) mountPoint=${OPTARG%/};;
	N) noMount=1;;
	r) recompute=1;;
	h|?) print_help;;
    esac
done

test -z $srcDir && die "ERROR: No source specified. Use -h for help"

dvdName=${srcDir##*/}
md5file="$md5dir/$dvdName.md5"

if (( noMount == 0 )); then
    mount $mountPoint || die "Error mounting $mountPoint"
fi

cd $mountPoint || die "Unable to enter $mountPoint"
mkdir -p $logDir || die "Unable to create log directory $logDir"
echo "Comparing files in $mountPoint to $srcDir" | tee $logDir/$dvdName.log

if [[ -f $md5file && $recompute == "0" ]]; then
    # Use existing md5 file
    md5sum -c $md5file 2>&1 | tee -a $logDir/$dvdName.log
else
    # compute md5 sums and check
    mkdir -p $md5dir || die "Unable to create md5 dir $md5dir"
    getmd5 $srcDir | tee $md5file | md5sum -c 2>&1 | tee -a $logDir/$dvdName.log
fi
cd

if (( noMount == 0 )); then
    umount $mountPoint || die "Error unmounting $mountPoint"
fi
