#! /bin/bash
# Created   : Thu 26 Jun 2008 04:59:12 PM PDT
# Modified  : Wed 10 Feb 2010 10:07:18 AM EST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

#LIGHT_BACKGROUND=1 exec mrxvt -name Bright -e vim $*
exec mrxvt -e vim $*
