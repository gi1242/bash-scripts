#! /bin/bash
# Created   : Wed 25 Jun 2014 10:39:39 PM EDT
# Modified  : Sun 03 Aug 2014 12:04:56 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Convert svn repos to git

function die() {
    echo "${ER}$*$RE" > /dev/stderr
    exit 1
}

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

[[ -z $1 ]] && \
    die "Usage svn-to-git <svndir> <gitremote> <description> <category-file>"

svndir=$1
gitremote=$2

svnroot=$(svn info $svndir | grep URL:)
svnroot=${svnroot##URL: }

[[ -z $svnroot ]] && die "No svnroot"

gitdir=${svndir}-git

# First backup original directory
echo "cp -a $svndir ~/tmp/svn-backup-$svndir-$$"
cp -a $svndir ~/tmp/svn-backup-$svndir-$$ || die "Could not backup"

git svn clone $svnroot $gitdir || die "Failed cloning"
rsync -av -HAX --exclude=.svn --exclude=.git $svndir/ $gitdir/ \
    || die "Error syncing"
( svn update && svn rm $svndir && svn commit -m 'Moved to git' && svn update ) \
    || die "Error removing $svndir"

[[ -d $svndir ]] && die "SVN dir $svndir still exists"

mv $gitdir $svndir || die "Error moving $gitdir to $svndir"
gitdir=$svndir

cd $gitdir || die "Could not change to $gitdir"
giturl=$2
if [[ -n $giturl ]]; then
    git remote add origin $giturl
    if ! git push origin master; then
	echo "${IT}Warning: Repo $giturl doesn't exist. Creating...$RE"
	server=${giturl%%:*}
	gitdir=${giturl##*:}
	ssh $server git init --bare $gitdir && git push origin master \
	    || die "Error pushing"
    fi
else
    echo "${IT}Warning: No git url specified, so no remote added.$RE"
fi

desc=$3
if [[ -n $desc ]]; then
    mkdir -p .gitweb
    echo "$desc" > .gitweb/description
    if [[ -n $4 ]]; then
	cp $4 .gitweb/category
    fi
    git add -Av .gitweb
    git commit -m 'Added gitweb description' && git push
else
    echo "${IT}Warning: No gitweb description${RE}"
fi

echo "${UL}Files not under version control${RE}"
git ls-files --others --exclude-standard
