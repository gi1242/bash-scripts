#! /bin/bash
# Created   : Sun 16 Sep 2012 03:20:57 PM EDT
# Modified  : Sun 16 Sep 2012 04:46:36 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Backup the SVN repository

function die()
{
    echo "$*" >> /dev/stderr

    exit 1
}

[[ -e /tmp/svn-backup.lock ]] && \
    die "Found lockfile /tmp/svn-backup.lock. Quitting."
touch /tmp/svn-backup.lock

svnroot=~/.svnroot
backupDir=~/ptmp/svnbackup
remoteDir=project:backup-svnroot

# Get a list of repos
repos=($(find $svnroot -wholename '*/db/current' -printf '%P\n'))
repos=(${repos[@]%/db/current})

for i in ${repos[@]}; do
    if [[ $i == */* ]]; then
	dstdir=$backupDir/${i%/*}
    else
	dstdir=$backupDir
    fi

    mkdir -p $dstdir
    svn-fast-backup $svnroot/$i $dstdir
done

rsync -Hav --delete $backupDir/ $remoteDir/

rm /tmp/svn-backup.lock
