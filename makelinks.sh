#! /bin/bash
# Created   : Sat 15 Aug 2009 02:13:25 PM EDT
# Modified  : Mon 20 Jan 2014 10:12:01 PM EST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>

# Get rid of symlinks in current path
cd $(pwd -P)

pkgName=$1
pkgName=${pkgName#pkgs/}
if [[ -d $PWD/pkgs ]]; then
    pkgDir=$PWD
else
    echo "Could not $PWD/pkgs"
    exit 1
fi
cd $pkgDir

if [[ -n "$1" && -d pkgs/$pkgName ]]; then
    pdirs=($(find pkgs/$pkgName -mindepth 1 -type d -printf '%P\n'))

    for d in "${pdirs[@]}"; do
	mkdir -p $d
	cd $d
	find $pkgDir/pkgs/$pkgName/$d -maxdepth 1 \( -type f -o -type l \) \
	    -exec ln -fsv {} . \;
	cd -
    done
fi

# Clean up symlinks
#symlinks -cdrs bin/ etc/ include/ info/ lib/ libexec/ sbin/ share/
shopt -s extglob
symlinks -cdrs @(bin|etc|include|info|lib|libexec|sbin|share)
