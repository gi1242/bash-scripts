#! /bin/bash
# Script to backup files using rsync

# Defaults
PROGNAME="rsync-minimal"
SRC_DIR="/home/"
DEST_PREFIX="/var/spool/backup/home"
MAX=5
LASTRUN="/var/state/$PROGNAME"
RSYNC_ARGS="-a --delete --delete-excluded"
EXCLUDE_FILE=""

function print_help() {
    echo "\
$PROGNAME [options] {--|--+} [rsync args]

rsync-minimal backs up the directory specified by -s using rsync. The
destination is determined by appending the version number to the prefix (-p).
By default the maximum number of versions is 5. The version currently sync-ed
is stored in a lastrun file (-r) [which is incremented every time this script
is run].

OPTIONS:

    -p <prefix>	prefix (default ${DEST_PREFIX})
    -n <N>	Number of versions (default ${MAX}). Use 1 for no versions.
    -x <file>	Exclude file (use NONE for no file)
    -s <dir>	Source dir (${SRC_DIR})
    -d <dir>	Same as -p <dir> -n 1
    -r <file>	Lastrun filename (${LASTRUN})
    -- <args>	Arguements for rsync.
    --+ <args>	Arguements for rsync in addition to $RSYNC_ARGS.
"
    exit 1
}

while getopts ":p:n:x:s:r:d:Dh-" OPT; do
    case $OPT in
	"p") DEST_PREFIX=${OPTARG:?"Option $OPT needs argument"};;
	"n") MAX=${OPTARG:?"Option $OPT needs argument"};;
	"x") EXCLUDE_FILE=${OPTARG:?"Option $OPT needs argument"};;
	"s") SRC_DIR=${OPTARG:?"Option $OPT needs argument"};;
	"d") DEST_PREFIX=${OPTARG:?"Option $OPT needs argument"}; MAX=1;;
	"r") LASTRUN=${OPTARG:?"Option $OPT needs argument"};;
	"D") DEBUG=1;;
	"h") print_help;;
	"-") break;;
	"?")
	    echo "Unrecognised option: $OPTARG"
	    print_help;;
    esac
done

for (( i=1; $i < $#; i++ )); do
    if [[ ${!i} == "--" ]]; then
	shift $i
	RSYNC_ARGS=$@
	break
    elif [[ ${!i} == "--+" ]]; then
	shift $i
	RSYNC_ARGS="${RSYNC_ARGS} $@"
	break
    fi
done

if (( $MAX == 1 )); then
    DEST_DIR=$DEST_PREFIX
elif [[ ! -e $LASTRUN ]]; then
    echo 0 > $LASTRUN || exit
    DEST_DIR="${DEST_PREFIX}0"
elif [[ -r $LASTRUN && -w $LASTRUN ]]; then
    i=$(< $LASTRUN)
    DEST_DIR="${DEST_PREFIX}$(( $i >= 0 && $i < $MAX ? $i : 0 ))"
    echo $(( ++i % $MAX )) > $LASTRUN
else
    echo "Error: $LASTRUN is not writable"
    exit
fi

if [[ $DEBUG ]]; then
    echo "Prefix:		$DEST_PREFIX"
    echo "Destination:	$DEST_DIR"
    echo "Max:		$MAX"
    echo "Exclude file:	$EXCLUDE_FILE"
    echo "Source dir:	$SRC_DIR"
    echo "Lastrun:	$LASTRUN"
    echo "Rsync args:	$RSYNC_ARGS"
fi

if [[ $EXCLUDE_FILE == "NONE" ]]; then
    nice rsync ${RSYNC_ARGS} ${SRC_DIR} ${DEST_DIR} 
elif [[ $EXCLUDE_FILE ]]; then
    nice rsync ${RSYNC_ARGS} --exclude-from ${EXCLUDE_FILE} ${SRC_DIR} ${DEST_DIR} 
else
    nice rsync ${RSYNC_ARGS} --exclude-from - ${SRC_DIR} ${DEST_DIR} <<- EOF
	# Named directories to backup
	+ src/
	+ math/
	+ config/
	- /*/*

	# Exclude CVS directories
	- CVS/

	# Exclude Audio / video
	- *.mp3
	- *.avi
	- *.mov
	- *.wmv
	- *.wav

	# Images
	- *.jpg
	- *.JPG
	- *.png
	- *.gif
	- *.xcf

	# Program outputs
	- *.pdf
	- *.log
	- *.dvi
	- *.aux
	- *.swp
	- *.o
	- *.pmc
	- *.plc
	- *.pyc
	- core

	# Archives
	*.tar
	*.tar.*
	*.tgz
	*.tbz2
	*.zip
	*.gz
	*.bz2

	# Backups
	- *.bak
	- *.orig*
	- *~

	# Don't backup files without extensions, unless I know what they are.
	# This will miss a few symlinks to directories, but I guess I can live
	# without that :)
	+ */
	+ *.*
	+ Makefile
	+ .fvwm/config
	- *
	EOF
fi
