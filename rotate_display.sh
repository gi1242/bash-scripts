#! /bin/bash

LVDS=LVDS
VGA=VGA

# 2010-02-05: With KMS, displays become suffixed with a "1".
if egrep -q '(nouveau|drm)fb' /proc/fb; then
    LVDS="${LVDS}1"
    VGA="${VGA}1"
fi

case $1 in
    inverted)
	xrandrRotationArg=inverted
	wacomRotationArg=HALF
	;;

    normal)
	xrandrRotationArg=normal
	wacomRotationArg=NONE
	;;

    left)
	xrandrRotationArg=left
	wacomRotationArg=CCW
	;;

    right)
	xrandrRotationArg=right
	wacomRotationArg=CW
	;;

    *)
	echo "Valid arguments are left, right, inverted, normal"
	exit 1
esac

#[[ -f ~/.invert_pgup_pgdn.xmodmap ]] && xmodmap ~/.invert_pgup_pgdn.xmodmaprc

xrandr --dpi 125 --output $LVDS --auto --rotate $xrandrRotationArg   \
    --output $VGA --off || exit 1

[[ -z "$WACOM_STYLUS" ]] && WACOM_STYLUS=stylus
[[ -z "$WACOM_ERASER" ]] && WACOM_ERASER=eraser
#for dev in $(xsetwacom list | cut -d' ' -f1); do
#    xsetwacom set $dev Rotate $wacomRotationArg || exit 1
#done

for i in ${!WACOM_*}; do
    xsetwacom set "${!i}" Rotate $wacomRotationArg || exit 1
done

tablet_rotation=$(xsetwacom get "$WACOM_STYLUS" Rotate)
[[ -f ~/.calibrate.$tablet_rotation ]] && source ~/.calibrate.$tablet_rotation

FvwmCommand Restart
