#! /bin/bash
# Created   : Mon 18 Feb 2008 02:54:00 AM PST
# Modified  : Mon 05 May 2008 01:15:46 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# The wacom utility to calibrate the tablet PC removes the "#!" line from the
# start of .xinitrc. After which I'm sure something will mess up! This script
# just runs wacom's utility, and then puts the #! line back into .xinitrc.
#
# I recommend you keep your ~/.xinitrc somewhere safe (e.g. say ~/.xstartup),
# and just source that file from your ~/.xinitrc. That way WACOM (or this
# script) can't mess things up too much.

mypath=${0%/*}
myprog=${0##*/}

if [[ $(which $myprog) == $mypath/$myprog ]]; then
    PATH=${PATH/$mypath/}
fi
$myprog

if head -n 1 ~/.xinitrc | grep -q "^#!"; then
    echo ".xinitrc not messed up by wacomcpl"
else
    xinitrc_contents=$(grep -v "^#!" ~/.xinitrc)
    modeline=$(grep "^#!" ~/.xinitrc)

    echo "$modeline" > ~/.xinitrc
    echo "$xinitrc_contents" >> ~/.xinitrc
fi
