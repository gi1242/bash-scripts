#! /bin/bash
# Created   : Sun 07 Oct 2012 10:31:26 PM EDT
# Modified  : Sun 07 Oct 2012 10:32:43 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

sessions=($(kdmctl list))

for s in "${sessions[@]}"; do
    display=${s%%,*}; s=${s#*,}
    vt=${s%%,*}; s=${s#*,}
    user=${s%%,*}; s=${s#*,}

    if [[ $user == jeneeiyer ]]; then
	kdmctl activate $vt
	exit 0
    fi
done

# No active session found, start a new one
kdmctl reserve 300
