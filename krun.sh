#! /bin/bash

function print_usage()
{
    echo "USAGE: krun <kinit_args> -- <program> <arguments>"
    echo
    echo If a credential cache is found, runs program. If not runs kinit, and
    echo then the program.

    exit 1
}

# If aklog is not installed, use /bin/true instead
which aklog > /dev/null 2>&1 && aklog=aklog || aklog=true

[[ $# == 0 ]] && print_usage

for (( i=1; $i <= $#; i++ )); do
    if [[ ${!i} == "--" ]]; then
	kinitArgs=("${@:1:$((i-1))}")
	execArgs=("${@:$((i+1))}")

	break
    fi
done

[[ -z "${execArgs[@]}" ]] && print_usage

if ! klist -s; then
    kinit "${kinitArgs[@]}" && ($aklog &)    # Get kerberos & AFS credentials
    if (( $? )); then
	echo Not executing "${execArgs[@]}".
	exit 1
    fi
elif which krenew &> /dev/null; then
    # Renew the ticket if it will expire in less x hours.
    #krenew -tH 1380		# 23 hours
    KINIT_PROG="$aklog &" krenew -tH 1380		# 23 hours
fi

if [[ $TERM == rxvt* && -z $KRUN_NOTITLE ]]; then
    echo -ne "\e]0;" >> /dev/tty
    echo -nE  "${execArgs[@]} (${PWD/#$HOME/~})" >> /dev/tty
    echo -ne "\a" >> /dev/tty
fi
exec "${execArgs[@]}"
