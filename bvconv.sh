#! /bin/bash
# Created   : Sat 13 Jun 2009 11:15:36 PM PDT
# Modified  : Mon 15 Jun 2009 04:42:06 PM PDT
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Encode movies produced by my digital camera

function die()
{
    echo "$ER: $*$RE" > /dev/stderr
    exit 1
}

function print_help()
{
    echo "\
${BD}USAGE$RE
    ${0##*/} ${AR}[options]$RE ${UL}file1.mov file1.mov ...$RE

Transcodes the given list of movies. (Designed to compress movies produced by
digital cameras)."
    exit 1
}

function eexec()
{
    echo "${IT}$@${RE}"
    if [[ -z $dryRun ]]; then
	eval "$*"
    fi
}

#Terminal color codes
RE=$'\e'"[0m"
ER=$'\e'"[0;31m"
BD=$'\e'"[0;36m"
UL=$'\e'"[0;32m"
IT=$'\e'"[0;33m"

# Defaults
deleteThumbnails=1
encoder=ffmpeg
vbitrate=1200
abitrate=128
odir=
dryRun=
threads=2
rotate=
flip=
quality=

while getopts ":ke:o:w:b:t:r:fq:n" OPT; do
    case $OPT in
	k) deleteThumbnails=0;;
	e) encoder=$OPTARG;;
	o) odir=$OPTARG;;
	w) vbitrate=$OPTARG;;
	b) abitrate=$OPTARG;;
	t) threads=$OPTARG;;
	r) 
	    case $OPTARG in
		cw)	rotate=1;;
		ccw)	rotate=2;;
		*)	rotate=$OPTARG;;
	    esac;;
	f) flip=1;;
	q) quality=$OPTARG;;
	n) dryRun=1;;

	\?)
	    echo "Unrecognised option: $OPTARG"
	    print_help;;
    esac
done

shift $((OPTIND-1))

if [[ ( -n "$rotate" || -n "$flip" || -n "$quality" ) && \
	$encoder != mencoder ]]; then
    echo "${ER}Rotation/flip/quality set. Using mencoder.${RE}"
    encoder=mencoder
fi

[[ -z "$quality" ]] && quality=realtime

[[ -n $odir ]] && mkdir -p $odir

for f in "$@"; do
    if [[ ${f##*.} =~ ^(jpg|JPG)$ ]]; then
	echo "${ER}Ignoring $f.$RE"
	continue
    else
	date "+${UL}(%T) Processing $f:$RE"
    fi

    output=${f%.*}.avi
    if [[ -n "$odir" ]]; then
	output="$odir/${output##*/}"
    elif [[ $output == $f ]]; then
	output="T-$output"
    fi

    [[ -e ${f%.*}.jpg ]] && eexec rm ${f%.*}.jpg

    case $encoder in
	transcode)
	    eexec transcode -i $f -o $output -y ffmpeg,tcaud -F mpeg4 \
		-w $vbitrate -b $abitrate || exit ;;

	ffmpeg)
	    eexec ffmpeg -y -i $f -threads $threads -acodec libmp3lame \
		-b ${vbitrate}k -ab ${abitrate}k $output || exit;;

	mencoder)
	    lavcopts="acodec=libmp3lame:vcodec=mpeg4:threads=$threads"
	    lavcopts="${lavcopts}:vbitrate=$vbitrate:abitrate=$abitrate"

	    # Quality options taken from the Mplayer libavcodec docs
	    case "$quality" in
		veryhigh)
		    lavcopts="${lavcopts}:mbd=2:mv0:trell:v4mv:cbp:last_pred=3"
		    lavcopts="${lavcopts}:predia=2:dia=2:vmax_b_frames=2"
		    lavcopts="${lavcopts}:vb_strategy=1:precmp=2:cmp=2:"
		    lavcopts="${lavcopts}:subcmp=2:preme=2:qns=2"
		    ;;

		high)
		    lavcopts="${lavcopts}:mbd=2:trell:v4mv:last_pred=2:dia=-1"
		    lavcopts="${lavcopts}:vmax_b_frames=2:vb_strategy=1:cmp=3"
		    lavcopts="${lavcopts}:subcmp=3:precmp=0:vqcomp=0.6:turbo"
		    ;;
		fast)
		    lavcopts="${lavcopts}:mbd=2:trell:v4mv:turbo"
		    ;;
		realtime)
		    lavcopts="${lavcopts}:mbd=2:turbo"
		    ;;
		*)
		    die "Quality must be high/fast/realtime"
		    ;;
	    esac

	    eexec mencoder $f -oac lavc -ovc lavc -lavcopts $lavcopts \
		-vf harddup${rotate:+:rotate=$rotate}${flip:+:flip} \
		-noskip -mc 0 -of lavf -o $output || exit;;
    esac
done
