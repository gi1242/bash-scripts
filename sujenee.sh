#! /bin/bash
# Created	: Tue 13 Dec 2005 11:40:29 AM CST
# Modified	: Mon 03 Apr 2006 07:27:39 PM CDT
# Author	: Gautam Iyer <gautam@math.uchicago.edu>
# Description	: If an X session for Jen�e is already running, then switch to
#		  it. Otherwise start a new X session for Jen�e.
#
# BUG: If my X session is started using KDM, then this does not launch a new X
# session for Jen�e.

# Check if Jen�e's already logged on
if pgrep -u jenee startx 2>&1 > /dev/null; then
    # Find the VT Jenee logged in from, and switch to it
    for i in 7 8 9 10; do
	if [[ $(stat -c %U /dev/tty$i 2>/dev/null ) == "jenee" ]]; then
	    # 2005-12-13: chvt gives some junk "permission denied" message if
	    # not run as root.
	    exec sudo chvt $i
	fi
    done

    exec echo "Could not find owning VT of Jenee's X session."
else
    # If we're not running from a TTY, then we should open an xterm window
    # TODO 2005-12-13: Figure out a free display number to use (instead of using
    # display 1).
    if tty -s; then
	# su won't run unless called from a TTY window.
	exec su - jenee -l -c "/usr/X11R6/bin/startx -- :1 > ~/.xsession-errors 2>&1 &"
    elif [[ -n $DISPLAY ]]; then
	exec mrxvt -name FvwmConsole --vt0.tabTitle "sujenee.sh" -e su - jenee -l -c "/usr/X11R6/bin/startx -- :1 2>&1 | tee ~/.xsession-errors"
    else
	exec echo "Not running in a TTY or X session."
    fi
fi
