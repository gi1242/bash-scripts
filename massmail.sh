#! /bin/bash
# Send an email to lots of people

while getopts "m:e:s:" OPT; do
    case $OPT in
	"m") msg=$OPTARG;;
	"e") emailFile=$OPTARG;;
	"s") subject=$OPTARG;;

	"?")
	    echo "Unrecognised option: $OPTARG"
	    exit;;
    esac
done

if [[ -z $emailFile || -z $subject ]]; then
    echo No email list or subject given
    exit 1
fi

if [[ -z $msg ]]; then
    # read from stdin
    msg=$(cat)
    if [[ -z $msg ]]; then
	echo No message
	exit 1;
    fi
fi

for i in $(<$emailFile); do
    echo "$msg" | mail -s "$subject" "$i"
done
