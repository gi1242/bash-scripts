#! /bin/bash
# Remove old GPG keys from my public keyring

fingerprints=$(gpg -k --with-colons | grep ^pub | grep -v gautam | \
    grep -v gi1242 | cut -d: -f5)

for i in $fingerprints; do
    gpg --batch --yes --delete-key $i
done
