#! /bin/zsh
# Created   : Mon 08 Jul 2019 04:53:00 PM EDT
# Modified  : Wed 02 Oct 2019 08:57:13 AM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'
#
# Run command in first tab of Konsole, and shell in all other tabs

SHELL=${SHELL:-/bin/zsh}
if [[ $KONSOLE_DBUS_SESSION == /Sessions/1 ]]; then
    print -Rn $'\e'"]0;$@ (${(%):-%~})"$'\a' > /dev/tty
    exec "$@"
elif [[ $KONSOLE_DBUS_SESSION == /Sessions/2 && "$@" == "sudo -s" ]]; then
    print -Rn $'\e'"]0;$screen -R -D (${(%):-%~})"$'\a' > /dev/tty
    exec screen -d -R
else
    exec $SHELL
fi
