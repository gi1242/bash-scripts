#! /bin/bash
# Created   : Fri 01 Jan 2010 01:37:49 AM EST
# Modified  : Thu 30 Dec 2010 01:01:53 PM EST
# Author    : Gautam Iyer <gi1242@users.sourceforge.net>
#
# Usage: nplaunch <hostname> options

hostname=$1
shift
ssh -f $hostname NPtcp $* &
sleep 3
NPtcp -h $hostname $*
