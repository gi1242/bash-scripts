This contains various shell scripts I've written **for myself** over the years.
Many are obsolete / useless now.
Some useful ones are listed here, mainly because the next time I want to do task X I can look here instead of grepping my scripts directory.

* **\*backup.sh**:
  Various different backup scripts.
  The basic idea is to use `rsync` to create a backup and make a snap shot using hard links.
  I wrote these in the days before [rsnapshot], and when I get to investigate rsnapshot more fully, I might switch if it provides comparable functionality.

* **makelinks.sh**: My local package manager based on symlinks. Install packages in `~/local/pkgs/`, and then this will create the appropriate symlinks in `~/local/bin` etc.

* **nplaunch.sh**: Time network speed. Wrapper around [NPtcp](http://bitspjoule.org/netpipe/).
  Note: Now using [iperf3](http://software.es.net/iperf/) is better.

* **svim.sh**: Helper script to start [vim] with the correct server name when I edit [LaTeX] documents to simplify inverse searches.

[rsnapshot]: http://www.rsnapshot.org/

[git]: http://git-scm.com/

[vim]: http://www.vim.org/

[LaTeX]: http://www.latex-project.org/
